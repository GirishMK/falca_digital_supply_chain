App.info({
  id: 'com.telsites.falca.admin',
  name: 'Admin',
  description: 'Online Bidding PlatForm',
  author: 'Palc Networks',
  email: 'info@palcnetworks.in',
  website: 'http://palcnetworks.com',
  version:"9"
});

App.accessRule("tel:*", { type: "intent" });
App.accessRule('*', { type: "navigation" });
App.accessRule('*', { type: "intent" });
App.accessRule('*');

App.icons({
  "android_mdpi": "icons/mipmap-mdpi/ic_launcher.png",
  "android_hdpi": "icons/mipmap-hdpi/ic_launcher.png",
  "android_xhdpi": "icons/mipmap-xhdpi/ic_launcher.png",
  "android_xxhdpi": "icons/mipmap-xxhdpi/ic_launcher.png",
  "android_xxxhdpi": "icons/mipmap-xxxhdpi/ic_launcher.png",

  // "android_mdpi":"icons/drawable-mdpi/ic_stat_palc.png",
  // "android_hdpi":"icons/drawable-hdpi/ic_stat_palc.png",
  // "android_xhdpi": "icons/drawable-xhdpi/ic_stat_palc.png",
  // "android_xxhdpi": "icons/drawable-xxhdpi/ic_stat_palc.png",
  // "android_xxxhdpi": "icons/drawable-xxxhdpi/ic_stat_palc.png"
});

App.launchScreens({
  "android_mdpi_portrait": "icons/drawable-mdpi/icon.png",
  "android_mdpi_landscape": "icons/drawable-mdpi/icon.png",
  "android_hdpi_portrait": "icons/drawable-hdpi/icon.png",
  "android_hdpi_landscape": "icons/drawable-hdpi/icon.png",
  "android_xhdpi_portrait": "icons/drawable-xhdpi/icon.png",
  "android_xhdpi_landscape": "icons/drawable-xhdpi/icon.png",
  "android_xxhdpi_portrait": "icons/drawable-xxhdpi/icon.png",
  "android_xxhdpi_landscape": "icons/drawable-xxhdpi/icon.png",
  "android_ldpi_portrait": "icons/drawable-ldpi/icon.png",
  "android_ldpi_landscape": "icons/drawable-ldpi/icon.png"
});
