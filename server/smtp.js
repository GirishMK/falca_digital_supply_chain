Meteor.startup(function () {

  process.env.ROOT_URL = Meteor.settings.public.ROOT_URL;
  
  Accounts.emailTemplates.siteName = Meteor.settings.public.siteName;

  Accounts.emailTemplates.from = Meteor.settings.public.email_from;

  Accounts.emailTemplates.resetPassword.subject = function (user) {
    return "FALCA password reset for " + user.profile.Name;
  };
  Accounts.urls.resetPassword = function (token) {
    return process.env.ROOT_URL + '/reset-password/' + token;
  }
  Accounts.emailTemplates.resetPassword.text = function (user, url) {
    return "Dear " + user.profile.Name + ",\n\n" +
      "Click the following link to reset your password.\n" +
      url + "\n\n" +
      "Please never forget it again!!!\n\n\n" +
      "Cheers,\n";
  };
  process.env.MAIL_URL = 'smtp://support%40falcasolutions.com:' + encodeURIComponent("Welcome@@1") + '@smtp.gmail.com:587';

});