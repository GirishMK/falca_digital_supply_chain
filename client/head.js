
import './head.html';
import '../imports/ui/components/noInternet/noInternet.js';
(function () {
    if (Meteor.isCordova) {
        document.addEventListener("deviceready", function () {
            //for internet Connection Checking
            document.addEventListener("offline", function () {
                try {                   
                    FlowRouter.go("/");
                    //set height of  popup as documnet height
                    $(".nointernet").show();
                    $(".nointernet").css({ "height": $(document).height() });
                }
                catch (e) {
                    console.log(e);
                }
            }, false);
            document.addEventListener("online", function () {
                try {                   
                    $(".nointernet").hide();
                    $(".nointernet").css({ "height": 0 });
                }
                catch (e) {
                    console.error(e);
                }
            }, false);
            //fire manually first time 
            if ($._data(document, "events") && (typeof ($._data(document, "events").offline) === "undefined" || $._data(document, "events").offline.length == 0)) {
                $(document).off("offline");
            }
            if ($._data(document, "events") && (typeof ($._data(document, "events").online) === "undefined" || $._data(document, "events").online.length == 0)) {
                $(document).off("online");
            }
        }, false);
    }
}());
