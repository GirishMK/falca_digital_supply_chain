import './login.html';
import { validateFields } from '/imports/api/extras/functions.js';
Template.login.events({
    'click #btnsignin': function (e) {
        try {
            $("input").removeClass("invalid");
            $(".errormessage").css("display", "none");
            var myNewTemplate = Template.spinner;
            var myContainer = document.getElementById('login-dropdown-list');
            var myRenderedTemplate = Blaze.render(myNewTemplate, myContainer);
            $('.overlay').css("display", "block");

            validateFields("email", "Email", "#login-useremail");
            validateFields("password", "Password", "#login-password");

            var useremail = $("#login-useremail").val();
            var password = $("#login-password").val();

            Meteor.loginWithPassword(useremail, password, function (a, b) {
                if (a) {
                    $('.overlay').css("display", "none");
                    Blaze.remove(myRenderedTemplate);
                    $('.toast').remove();
                    Materialize.toast('Enter Valid Username and Password.', 3000);
                    return;
                }
                else {
                    if (Meteor.user().profile.Role === "SuperAdmin" || Meteor.user().profile.Role === "Admin" || Meteor.user().profile.Role === "StoreManager") {
                        $('.overlay').css("display", "none");
                        Blaze.remove(myRenderedTemplate);
                        FlowRouter.go('/home');
                    } else {
                        Meteor.logout();
                        $('.overlay').css("display", "none");
                        Blaze.remove(myRenderedTemplate);
                        $('.toast').remove();
                        Materialize.toast('You are not a valid User.', 3000);
                        return;
                    }
                }
            });
        } catch (error) {
            $('.overlay').css("display", "none");
            Blaze.remove(myRenderedTemplate);
        }
    },
    'keypress #btnsignin': function (e) {
        var key = e.which;
        if (key == 13) {
            $('#resetPassword').click();
            return false;
        }
    }
})