import './home.html';

import {
    tblPublish
} from "/imports/api/tblPublish/tblPublish.js";

Template.homepage.onCreated(function () {
    Meteor.subscribe('publish.unassigned.aadhar.count');
    Meteor.subscribe('publish.unassigned.aadharimage.count');
    Meteor.subscribe('publish.assigned.aadhar.count');
    Meteor.subscribe('publish.assigned.aadharimage.count');
    Meteor.subscribe('publish.unassigned.consumer.count');
    Meteor.subscribe('publish.unassigned.nonperishable.count');
    Meteor.subscribe('publish.assigned.nonperishable.count');
    Meteor.subscribe('publish.unassigned.perishable.count');
    Meteor.subscribe('publish.assigned.perishable.count');
    Meteor.subscribe('publish.unassigned.nonperishable.load.count');
    Meteor.subscribe('publish.unassigned.perishable.load.count');
    Meteor.subscribe('publish.assigned.nonperishable.load.count');
    Meteor.subscribe('publish.assigned.perishable.load.count');
    Meteor.subscribe('publish.unassigned.nonperishable.unload.count');
    Meteor.subscribe('publish.unassigned.perishable.unload.count');
    Meteor.subscribe('publish.assigned.nonperishable.unload.count');
    Meteor.subscribe('publish.assigned.perishable.unload.count');
    Meteor.subscribe('publish.verified.consumer.count');

    //Store Manager card count 
    Meteor.subscribe('publish.GowdownInventory.count');
    Meteor.subscribe('publish.preOrderInventory.count');
    Meteor.subscribe('publish.shippingorders.count');
    Meteor.subscribe('publish.LocalTransport.count');
    Meteor.subscribe('publish.salesInventory.count');
    Meteor.subscribe('publish.productCancel.count');

    Meteor.subscribe('publish.nonperishable', ["AvailableToAdmin", "BidAccepted"]);

    if (Meteor.isCordova) {
        document.addEventListener("deviceready", function () {
            try {
                var push = PushNotification.init({
                    android: {
                        senderID: 474397545675,
                        forceShow: true,
                        icon: "ic_stat_palc"
                    }
                });
                push.on('notification', function (e) {
                    try {
                        if (e.additionalData && e.additionalData.url) {
                            window.localStorage.setItem("pushUrl", JSON.stringify(e.additionalData.url));
                        }

                    } catch (e) {
                        console.error(e);
                    }
                });
                push.on('error', function (e) {
                    alert(e);
                    console.log(e);
                });
                //Removed unnecessasary typeof... we can directly compare any object with undefined.
                if (Meteor.userId()) {
                    push.on('registration', function (data) {
                        try {
                            var GCMID = data.registrationId;
                            if (GCMID) {
                                //Save On Server 
                                Meteor.call("AddGCMID", GCMID, function (status) {
                                    console.log(status);
                                });
                            }
                        } catch (e) {
                            console.error(e);
                        }
                    });
                }
            } catch (e) {
                console.log(e);
            }
        }, false);
    }
});

Template.homepage.helpers({
    "unassignedFarmer_count": function () {
        return Counts.get("publish.unassigned.aadhar.count") + Counts.get("publish.unassigned.aadharimage.count");
    },
    "assignedFarmer_count": function () {
        return Counts.get("publish.assigned.aadhar.count") + Counts.get("publish.assigned.aadharimage.count");
    },
    "unassignedProduct_count": function () {
        return Counts.get("publish.unassigned.nonperishable.count") + Counts.get("publish.unassigned.perishable.count");
    },
    "assignedProduct_count": function () {
        return Counts.get("publish.assigned.nonperishable.count") + Counts.get("publish.assigned.perishable.count");
    },
    "unassignedLoad_count": function () {
        return Counts.get("publish.unassigned.nonperishable.load.count") + Counts.get("publish.unassigned.perishable.load.count");
    },
    "assignedLoad_count": function () {
        return Counts.get("publish.assigned.nonperishable.load.count") + Counts.get("publish.assigned.perishable.load.count");
    },
    "unassignedUnload_count": function () {
        return Counts.get("publish.unassigned.perishable.unload.count");
    },
    "assignedUnload_count": function () {
        return Counts.get("publish.assigned.perishable.unload.count");
    },
    "unassignedConsumer_count": function () {
        return Counts.get("publish.unassigned.consumer.count");
    },
    "verifiedConsumer_count": function () {
        return Counts.get("publish.verified.consumer.count");
    },
    //Sales Manager Count
    "godown_inventory_count": function () {
        return Counts.get("publish.GowdownInventory.count");
    },
    "preorder_inventory_count": function () {
        return Counts.get("publish.preOrderInventory.count");
    },
    "shipping_order_count": function () {
        return Counts.get("publish.shippingorders.count");
    },
    "start_transport_count": function () {
        return Counts.get("publish.LocalTransport.count");
    },
    "sales_inventory_count": function () {
        return Counts.get("publish.salesInventory.count");
    },
    "product_post_cancel_count": function () {
        return Counts.get("publish.productCancel.count");
    },
    "yet_tobid_products": function () {
        return tblPublish.find({
            "Status": "AvailableToAdmin",
            "ShipProductTo": "Godown",
            "TotalBids": 0
        }).count();
    },
    "bids_in_progress": function () {
        return tblPublish.find({
            "Status": "AvailableToAdmin",
            "ShipProductTo": "Godown",
            "TotalBids": {
                $gte: 1
            }
        }).count();
    }
})
Template.homepage.events({
    "click #aadhar_approval_card": function () {
        if (Meteor.userId()) {
            FlowRouter.go('/registeredFarmer')
        } else {
            return false;
        }
    },
    "click #product_approval_card": function () {
        if (Meteor.userId()) {
            FlowRouter.go('/postedProduct')
        } else {
            return false;
        }
    },
    "click #load_approval_card": function () {
        if (Meteor.userId()) {
            FlowRouter.go('/loadProduct')
        } else {
            return false;
        }
    },
    "click #unload_approval_card": function () {
        if (Meteor.userId()) {
            FlowRouter.go('/unloadProduct')
        } else {
            return false;
        }
    },
    "click #consumer_approval_card": function () {
        if (Meteor.userId()) {
            FlowRouter.go('/consumercard')
        } else {
            return false;
        }
    },

    "click #godown_bids_card": function () {
        if (Meteor.userId()) {
            FlowRouter.go('/bidProducts')
        } else {
            return false;
        }
    },

    "click #godown_inventory": function () {
        if (Meteor.userId()) {
            FlowRouter.go('/godownInventory')
        } else {
            return false;
        }
    },
    "click #preorder_inventory": function () {
        if (Meteor.userId()) {
            FlowRouter.go('/preorderInventory')
        } else {
            return false;
        }
    },
    "click #shipping_order": function () {
        if (Meteor.userId()) {
            FlowRouter.go('/shippingOrder')
        } else {
            return false;
        }
    },
    "click #start_transport": function () {
        if (Meteor.userId()) {
            FlowRouter.go('/startTransport')
        } else {
            return false;
        }
    },
    "click #sales_inventory": function () {
        if (Meteor.userId()) {
            FlowRouter.go('/salesInventory')
        } else {
            return false;
        }
    },
    "click #product_post_cancel": function () {
        if (Meteor.userId()) {
            FlowRouter.go('/ProductPostCancel')
        } else {
            return false;
        }
    },
});
$(document).ready(function () {
    if (Meteor.isCordova) {
        document.addEventListener("deviceready", function () {
            var updateUrl = Meteor.settings.public.ApkLink + "/AppUpdate/AdminApp/AdminApp.xml";
            window.AppUpdate.checkAppUpdate(function (e) {
                console.log(e);
            }, function (e) {
                console.log(e);
            }, updateUrl);
        }, false);
    }

});