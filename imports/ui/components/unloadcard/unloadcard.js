import './unloadcard.html';

Template.unloadCard.onCreated(function () {    
    // Meteor.subscribe("publish.users.profile");
    Meteor.subscribe('publish.unassigned.nonperishable.unload.count');
    Meteor.subscribe('publish.unassigned.perishable.unload.count');
    Meteor.subscribe('publish.assigned.nonperishable.unload.count');
    Meteor.subscribe('publish.assigned.perishable.unload.count');
});

Template.unloadCard.helpers({
    "unassignNonPerishableProducts": function () {
        return Counts.get("publish.unassigned.nonperishable.unload.count");        
    },
    "unassignPerishableProducts": function () {
        return Counts.get("publish.unassigned.perishable.unload.count");        
    },
    "assignedNonPerishableProducts": function () {
        return Counts.get("publish.assigned.nonperishable.unload.count");        
    },
    "assignedPerishableProducts": function () {
        return Counts.get("publish.assigned.perishable.unload.count");       
    },
    templateGestures: {
        'swipeleft #pending': function () {
            $(".approved").trigger("click");
        },
        'swiperight #approved': function () {
            $(".pending").trigger("click");
        }
    },
})
Template.unloadCard.events({
    'click .unassignNonPerishable': function () {
        if (Meteor.userId()) {
            FlowRouter.go('/unassignUnloadNonPerishable')
        } else {
            return false;
        }
    },
    'click .unassignPerishable': function () {
        if (Meteor.userId()) {
            FlowRouter.go('/unassignUnloadPerishable')
        } else {
            return false;
        }
    },
    'click .assignNonPerishable': function () {
        if (Meteor.userId()) {
            FlowRouter.go('/assignUnloadNonPerishable')
        } else {
            return false;
        }
    },
    'click .assignPerishable': function () {        
        if (Meteor.userId()) {
            FlowRouter.go('/assignUnloadPerishable')
        } else {
            return false;
        }
    }
})