import './notification.html';
import { tblNotification } from '/imports/api/tblNotification/tblNotification.js';
import { tblPublish } from '/imports/api/tblPublish/tblPublish.js';
import { Product_States, MAXIMUM_PRODUCTS_LIMIT_NOTIF, Perishable_States, Preorders_States } from '/imports/api/extras/constants.js';
// Template setup starts
Template.notification.onCreated(function () {
    Meteor.subscribe("notifications.all");
    var statusArray = [Product_States[1], Product_States[4]];
    Meteor.subscribe('publish.perishable', statusArray);
    var statusArray = [Product_States[1], Product_States[4]];
    Meteor.subscribe('publish.nonperishable', statusArray);
    var perishabestatusArray = [Perishable_States[5], Perishable_States[6]];
    Meteor.subscribe('perishableTransport', perishabestatusArray);
    Session.set('skip_recpost', 0);
    Session.set('limit_recpost', 1);
    Meteor.subscribe("preorders.all");
})
// Template setup ends

// Helpers starts
Template.notification.helpers({
    "notifications": function () {
        return tblNotification.find({
            "Recipeints.UserId": Meteor.userId(),
            // "Recipeints.IsRead": false
        },
            {
                'limit': MAXIMUM_PRODUCTS_LIMIT_NOTIF * Session.get('limit_recpost'),
                'sort': {
                    "CreatedAt": -1
                }
            });
    },
    notificationsCount: function () {
        let count = tblNotification.find({
            "Recipeints.UserId": Meteor.userId(),
        }).count();
        return (count > 10);
    },
    generateURL: function (notif_id) {
        var notif = tblNotification.find({ '_id': notif_id }).fetch()[0];
        var notif_message = notif.Message;
        if (Meteor.userId()) {
            if (notif) {
                switch (notif_message.Type) {
                    case "Product_Posted":
                        // Fetching aadhar number
                        var publish = tblPublish.find({
                            "_id": new Mongo.ObjectID(notif_message.Id),
                            "Status": Product_States[1],
                        }).fetch()[0];
                        if (publish) {
                            if (publish.Perishable === "No") {
                                FlowRouter.go('/unassignNonPerishableProduct');
                                $('html, body').animate({
                                    scrollTop: $("#" + notif_message.Id).offset().top
                                }, 1000);
                            } else {
                                FlowRouter.go('/unassignPerishableProduct');
                                $('html, body').animate({
                                    scrollTop: $("#" + notif_message.Id).offset().top
                                }, 1000);
                            }
                        } else {
                            // implement redirection to approved page
                            $('.toast').remove();
                            Materialize.toast("Product has already been assigned.", 3000);
                        }
                        break;
                    case "Bid_Accepted":
                        // Fetching aadhar number
                        var publish = tblPublish.find({
                            "_id": new Mongo.ObjectID(notif_message.Id),
                            "Status": Product_States[4],
                        }).fetch()[0];
                        if (publish) {
                            FlowRouter.go('/unassignLoadNonPerishable');
                            $('html, body').animate({
                                scrollTop: $("#" + notif_message.Id).offset().top
                            });
                        } else {
                            // implement redirection to approved page
                            $('.toast').remove();
                            Materialize.toast("Product has already been loaded.", 3000);
                        }
                        break;

                    // implement redirection to Preorder page
                    case "preorder_Assign_Admin":
                        var transportObj = tblPreorders.find({
                            "_id": new Mongo.ObjectID(notif_message.Id),
                            "Status": Preorders_States[1]
                        }).fetch()[0];
                        if (transportObj) {
                            route = "/preorderInventory"
                            FlowRouter.go(route);
                        }
                        else {
                            $('.toast').remove();
                            Materialize.toast("Preorder has already been assigned.", 3000);
                        }
                        break;

                    // implement redirection to Godownpage page

                    case "Product_Added_to_inventory":
                        var transportObj = tblTransportPerishable.find({
                            "_id": new Mongo.ObjectID(notif_message.Id),
                            "Status": { $in: [Perishable_States[5], Perishable_States[6]] }
                        }).fetch()[0];
                        route = "/godownInventory"
                        FlowRouter.go(route);
                        break;

                    // implement redirection to shippingOrder page

                    case "Sales_Confirmed_Admin":
                        var preordertObj = tblPreorders.find({
                            "_id": new Mongo.ObjectID(notif_message.Id),
                            "Status": Preorders_States[2]
                        }).fetch()[0];
                        if (preordertObj) {
                            route = "/shippingOrder"
                            FlowRouter.go(route);
                        }
                        else {
                            $('.toast').remove();
                            Materialize.toast("Preorder has already been Shipped.", 3000);
                        }
                        break;
                }
            }
        }
    }
})
Template.notification.events({
    "click .notititle": function (e) {
        var notif_id = $(e.currentTarget).data('pid');
        Template.notification.__helpers.get('generateURL')(notif_id);
    },
    'click .loadmore': function (error) {
        var y = Session.get('limit_recpost') + 1;
        Session.set('limit_recpost', y);
        var x = tblNotification.find({
            "Recipeints.UserId": Meteor.userId(),
        }).count();
        if (x > Session.get('skip_recpost') + MAXIMUM_PRODUCTS_LIMIT_NOTIF) {
            Session.set('skip_recpost', Session.get('skip_recpost') + MAXIMUM_PRODUCTS_LIMIT_NOTIF);
            if (x > Session.get('skip_recpost') + MAXIMUM_PRODUCTS_LIMIT_NOTIF) {
                $(".loadmore").show();
            } else {
                $(".loadmore").hide();
            }
        }
    }
});
// Helpersd ends