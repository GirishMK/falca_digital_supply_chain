import './header.html';
import '../../components/profile/profile.js';;
import '../../components/productDetail/productDetail.js';
import '../../components/profile/profile.js';
import { tblNotification } from '/imports/api/tblNotification/tblNotification.js';
Template.header.onCreated(function () {
    Meteor.subscribe("notifications.all");
})
Template.header.onRendered(function () {
    Meteor.setTimeout(function () {
        $('.collapsible').collapsible();
    }, 1000)
})
// Helpers starts
Template.header.helpers({
    "count_Notifications": function () {
        if (Meteor.userId()) {
            return tblNotification.find({
                Recipeints: {
                    $elemMatch: {
                        UserId: Meteor.userId(),
                        IsRead: false
                    }
                }
                // "Recipeints.UserId": Meteor.userId(),
                // "Recipeints.IsRead": false
            }).count();
        }
    },
});
// Template setup starts

// Template setup ends
Template.header.events({
    'click #noticount': function () {
        Meteor.call("notificationReaded");
    },
})