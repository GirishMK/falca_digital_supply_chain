import "./consumercard.html";

Template.productCard.onCreated(function () {
    Meteor.subscribe("publish.users.profile");
    Meteor.subscribe('publish.verified.consumer.count');
})

Template.consumerCard.helpers({
    "unassignedConsumer_count": function () {
        return Counts.get("publish.unassigned.consumer.count");
    },
    "verifiedConsumer_count": function () {
        return Counts.get("publish.verified.consumer.count");
    },
})
Template.consumerCard.events({
    'click .registeredconsumer':function(){
        if (Meteor.userId()) {
            FlowRouter.go('/consumerVerification')
        } else {
            return false;
        }
    },
    'click .verifiedconsumer':function(){
        if (Meteor.userId()) {
            FlowRouter.go('/consumerList')
        } else {
            return false;
        }
    }
})