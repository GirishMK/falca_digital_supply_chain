import "./productDetail.html";
import { tblProducts } from '../../../api/tblProducts/tblProducts.js';
import { MAXIMUM_PRODUCTS_LIMIT } from '/imports/api/extras/constants.js';
// Template onCreated setup starts
Template.productDetail.onCreated(function () {
	Session.set('skip_recpost', 0);
	Session.set('limit_recpost', 1);
	Meteor.subscribe("products.all");
})
// Template onCreated setup ends

// Helpers Starts
Template.productDetail.helpers({
	"products": function () {
		return tblProducts.find({}, {
			'limit': MAXIMUM_PRODUCTS_LIMIT * Session.get('limit_recpost'),
			sort: { "Name": 1 }
		});
	},
	"productsCount": function () {
		let count = 0;
		count = tblProducts.find({}).count();
		if (count > 5) {
			return true;
		} else {
			return false;
		}
	}
})
// Helpers ends

// Events starts
Template.productDetail.events({
	"click #delete_product": function (event, context) {
		var productId = this._id._str;
		$('#confirmModal').modal('open', { dismissible: false });
		$(".deleteProduct").attr("data-proid", productId);
	},
	"click #disable": function (event, context) {
		var productId = this._id._str;
		Meteor.call("disable.product", productId, function (error, success) {
			if (error) {
				console.error(error);
				throw error;
			} else {
				if (success) {
					$('.toast').remove();
					Materialize.toast('Product Disabled.', 3000);
				}
			}
		});
	},
	"click #enable": function (event, context) {
		var productId = this._id._str;
		Meteor.call("enable.product", productId, function (error, success) {
			if (error) {
				console.error(error);
				throw error;
			} else {
				if (success) {
					$('.toast').remove();
					Materialize.toast('Product Enabled.', 3000);
				}
			}
		});
	},
	'click .loadmore': function (error) {
		var y = Session.get('limit_recpost') + 1;
		Session.set('limit_recpost', y);
		var x = tblProducts.find({}).count();
		if (x > Session.get('skip_recpost') + MAXIMUM_PRODUCTS_LIMIT) {
			Session.set('skip_recpost', Session.get('skip_recpost') + MAXIMUM_PRODUCTS_LIMIT);
			if (x > Session.get('skip_recpost') + MAXIMUM_PRODUCTS_LIMIT) {
				$(".loadmore").show();
			} else {
				$(".loadmore").hide();
			}
		}
	}
})
// Event Ends