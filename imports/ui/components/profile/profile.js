import "./profile.html";
import {validateFields } from '/imports/api/extras/functions.js';
Template.profile.onCreated(function () {
    this.editphone = new ReactiveVar(true);
});
Template.profile.onRendered(function () {
    // not allowing to paste in inputs or textareas
    $('input,textarea').bind('paste', function (e) { e.preventDefault(); });
});
Template.profile.helpers({
    "editphone": function () {
        return Template.instance().editphone.get();
    },
    templateGestures: {
        'swipeleft #personal-info': function () {
            $(".contact").trigger("click");
        },
        'swiperight #contact-info': function () {
            $(".personal").trigger("click");
        }
    },
});
Template.profile.events({
    "click #editphone": function (event, context) {
        context.editphone.set(!context.editphone.get());
        $("#updatephone").addClass("enablebutton");
    },
    'click #updatephone': function () {
        try {
            $("input").removeClass("invalid");
            $(".errormessage").css("display", "none");
            $('.overlay').css("display", "block");
            var myNewTemplate = Template.spinner;
            var myContainer = document.getElementById('profileSpin');
            var myRenderedTemplate = Blaze.render(myNewTemplate, myContainer);

            validateFields("phone", "Phone Number", "#txtphone");
            if (($("#txtphone").val()) === (Meteor.user().profile.Phone.Primary)) {
                $('.overlay').css("display", "none");
                Blaze.remove(myRenderedTemplate);
                $('.toast').remove();
                Materialize.toast('Please enter a new number', 3000);
                $("#txtphone").focus();
                $("#txtphone").addClass("custs_errors");
                return;
            }
            var update = {
                "Phone": $("#txtphone").val(),
            };
            Meteor.call('UpdateProfile', update, function (a) {
                if (a) {
                    $('.overlay').css("display", "none");
                    Blaze.remove(myRenderedTemplate);
                    $('.toast').remove();
                    Materialize.toast('Profile Update Failed!!!!', 3000);
                } else {
                    $('.overlay').css("display", "none");
                    Blaze.remove(myRenderedTemplate);
                    $('.toast').remove();
                    Materialize.toast('Profile Details Updated Successfully', 3000);
                    FlowRouter.go("/");
                }
            });
        }
        catch (error) {
            $('.overlay').css("display", "none");
            Blaze.remove(myRenderedTemplate);
        }
    }
});