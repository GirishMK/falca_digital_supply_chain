import './productcard.html';

Template.productCard.onCreated(function () {
    Meteor.subscribe('publish.unassigned.nonperishable.count');
    Meteor.subscribe('publish.assigned.nonperishable.count');
    Meteor.subscribe('publish.unassigned.perishable.count');
    Meteor.subscribe('publish.assigned.perishable.count'); 
})
Template.productCard.helpers({
    unassignedNonPerishable: function () {
        return Counts.get("publish.unassigned.nonperishable.count");
    },
    assignedNonPerishable: function () {
        return Counts.get("publish.assigned.nonperishable.count");
    },
    unassignedPerishable: function () {
        return Counts.get("publish.unassigned.perishable.count");
    },
    assignedPerishable: function () {
        return Counts.get("publish.assigned.perishable.count");
    },    
    templateGestures: {
        'swipeleft #pending': function () {
            $(".approved").trigger("click");
        },
        'swiperight #approved': function () {
            $(".pending").trigger("click");
        }
    },
})

Template.productCard.events({
    'click .unassignnonperishable': function () {
        if (Meteor.userId()) {
            FlowRouter.go('/unassignNonPerishableProduct')
        } else {
            return false;
        }
    },
    'click .unassignperishable': function () {
        if (Meteor.userId()) {
            FlowRouter.go('/unassignPerishableProduct')
        } else {
            return false;
        }
    },
    'click .assignnonperishable': function () {
        if (Meteor.userId()) {
            FlowRouter.go('/assignNonPerishableProduct')
        } else {
            return false;
        }
    },
    'click .assignperishable': function () {
        if (Meteor.userId()) {
            FlowRouter.go('/assignPerishableProduct')
        } else {
            return false;
        }
    }
})