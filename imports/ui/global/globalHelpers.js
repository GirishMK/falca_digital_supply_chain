// This file will be consist of all the globally accessible functions
// Please update the function list in the comments
// 1. add - to add same type of not null and not undefined values
// 2. ago - return the more readable format of time from a date object
// 3. lastIndex - returns true if the current index is last index of the array

// Function: add
// Parameters: a, b
// Return: a + b
Template.registerHelper("add", function (a, b) {
    if (a !== undefined && a !== null && b !== undefined && b !== null && typeof (a) === typeof (b)) {
        return a + b;
    } else {
        throw "TYPE_MISMATCH_ERROR";
    }
});

// Function: ago
// Parameters: date
// Return: readable format of date
Template.registerHelper("ago", function (date) {
    check(date, Date);
    return moment(date).fromNow();
});

// Function: lastIndex
// Parameters: array, index
// Return: true or false
Template.registerHelper("lastIndex", function (array, index) {
    if (index === parseInt(index, 10) && array.constructor === Array) {
        return array.length === index + 1;
    } else {
        throw "TYPE_MISMATCH_ERROR";
    }
});

// Function: equal
// Parameters: two variables
// Return: true or false
Template.registerHelper("GlobalEqual", function (a, b) {
    return a === b;
});
Template.registerHelper("GlobalAreNotEqual", function (a, b) {
    return a !== b;
});

// Function: printArray
// Parameters: an array
// Return: Stringify an array
Template.registerHelper("printArray", function (array) {
    return array.toString().replace(",", ", ")
})


Template.registerHelper("GlobalformattedDate", function (mongodate) {
    return moment(mongodate).format("DD/MM/YYYY");
});

// Function: multiply
// Parameters: a, b
// Return: a * b
Template.registerHelper("multiply", function (a, b) {
    if (a !== undefined && a !== null && b !== undefined && b !== null && typeof (a) === "number" && typeof (b) === "number") {
        return a * b;
    } else {
        throw "TYPE_MISMATCH_ERROR";
    }
});

Template.registerHelper("returnOneIfEmpty", function(a,b){
    if(a !== ""){
        return a;
    }else{
        return b;
}
});
Template.registerHelper("Greaterthan", function (a, b) {    
    return a > b;
});