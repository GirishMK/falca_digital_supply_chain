import './footer.html';
var count = 0;

Template.foot.onRendered(function () {
    count = 0;
});
Template.foot.onDestroyed(function () {
    count = 0;
});
Template.foot.onCreated(function () {
    if (Meteor.isCordova) {
        try {
            var callback = function (e) {
                try {
                    e.preventDefault();
                    e.stopPropagation();
                    //Menu Open Close Menu
                    if ($("#sidenav-overlay").length > 0) {
                        $('.button-collapse').sideNav('hide');
                        return;
                    } 
                    if($('div').hasClass('modal-overlay')){
                        $(".modal-overlay").css("display", "none");
                       }                   
                    count++;
                    if ((window.location.pathname === "/home" || window.location.pathname === "/login" ) && count > 1) {
                        if (count > 2) {
                            navigator.app.exitApp();
                        } else {
                                $('.toast').remove();
                                Materialize.toast('Press two times to exit the app', 3000, 'rounded toast-message');
                        }
                    }
                } catch (e) {
                    console.error(e);
                }
            };
            var pushUrlLink = function () {
                try {
                    var pushUrl = window.localStorage.getItem("pushUrl");
                    //url start 
                    if (pushUrl && pushUrl !== "undefined") {
                        window.localStorage.setItem("pushUrl", undefined);
                        var data = JSON.parse(pushUrl);
                        var goToUrl = "";
                        if (data.page && FlowRouter._routes) {
                            for (var key in FlowRouter._routes) {
                                var curentUrl = FlowRouter._routes[key];
                                if (curentUrl.path === data.page) {
                                    goToUrl = curentUrl.path;
                                }
                            }
                        }
                        if (typeof goToUrl !== "undefined" && goToUrl !== "") {
                            switch (data.page) {
                                case "/unassignPerishableProduct":
                                    FlowRouter.go("/unassignPerishableProduct");
                                    $('html, body').animate({
                                        scrollTop: $("#"+data.productId).offset().top
                                  }, 1000);                       
                                    break;
                                case "/unassignNonPerishableProduct":
                                    FlowRouter.go("/unassignNonPerishableProduct");
                                    $('html, body').animate({
                                        scrollTop: $("#"+data.productId).offset().top
                                  }, 1000); 
                                    break;
                                default:
                                    FlowRouter.go("/home");
                            }
                        }
                    }
                    //normal start 
                } catch (e) {
                    console.error(e);
                    FlowRouter.go("/");
                }
            }
            if ($._data(document, "events") && (typeof ($._data(document, "events").backbutton) === "undefined" || $._data(document, "events").backbutton.length == 0)) {
                $(document).off("backbutton", callback).on("backbutton", callback);
            }
            if ($._data(document, "events") && (typeof ($._data(document, "events").resume) === "undefined" || $._data(document, "events").resume.length == 0)) {
                $(document).off("resume", pushUrlLink).on("resume", pushUrlLink);
            }
        }
        catch (e) {
            console.error(e);
        }
    }
});