// All tblProductVarities-related publications

import { Meteor } from 'meteor/meteor';
import { tblNotification } from '../tblNotification.js';

// Publishing all the documents inside tblPublish
Meteor.publish('notifications.all', function () {
	return tblNotification.find();
});