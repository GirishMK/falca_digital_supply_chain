import { tblNotification } from './tblNotification.js';
import { check } from 'meteor/check';
import { Mongo } from 'meteor/mongo';
import { log } from "/imports/startup/both/index.js";

Meteor.methods({
	//Function to send notifications to everyone
	// If you want to send notification to all consumers set allConsumers to true
	// If you want to send notification to all farmers set allFarmers to true
	// If you want to send notification to all backend set allBackend to true
	// If you want to send notification to all admins set allAdmin to true	
	// If you have a set of ids then send them to other_arr in a string arr
	"send.notifications": function (message, other_arr, allConsumers = false, allFarmers = false, allBackend = false, allAdmins) {
		try {
			if (other_arr.length) {
				for (var i = 0; i < other_arr.length; i++) {
					other_arr[i] = {
						"UserId": other_arr[i],
						"IsRead": false
					}
				}
			}

			var roles = [];
			if (allConsumers) roles.push("Consumer");
			if (allFarmers) roles.push("Farmer");
			if (allBackend) roles.push("Falcaperson");
			if (allAdmins) roles.push("Admin");

			log.debug("Sending inApp notification. | To: All( " + roles.toString() + ") and [" + other_arr.toString() + "]", {}, Meteor.userId());
			//Fetching all the ids to send the notifications and appending it to other_arr
			if (roles) {
				var users = Meteor.users.find({ "profile.Role": { $in: roles } }, { _id: 1 }).fetch();
				for (var i = 0; i < users.length; i++) {
					other_arr.push({
						"UserId": users[i]._id,
						"IsRead": false
					});
				}
			}

			if (other_arr) {
				let notificationID = tblNotification.insert({
					"Recipeints": other_arr,
					"Message": message,
					"SenderId": Meteor.userId(),
					"CreatedAt": new Date()
				});
				log.info("Success: In app notification sent successfully. | To: All( " + roles.toString() + ") and [" + other_arr.toString() + "]", {}, Meteor.userId());
				return notificationID;
			} else {
				log.warn("Warning: InApp notification sent failed. | Reason: No recipients to send.", {}, Meteor.userId());
			}
		} catch (error) {
			log.error("Error: InApp notification sent failed.  | To: All( " + roles.toString() + ") and [" + other_arr.toString() + "]", {}, Meteor.userId());
		}
	},
	notificationReaded: function () {
		if (Meteor.userId()) {
			log.debug("Changing all in app notification to read.", {}, Meteor.userId());
			try {
				if (Meteor.userId()) {
					tblNotification.update(
						{
							Recipeints: {
								$elemMatch: {
									UserId: Meteor.userId(),
									IsRead: false
								}
							}
						},
						{
							$set: {
								"Recipeints.$.IsRead": true
							}
						},
						{
							multi: true
						}
					)
					log.info("Success: All In app notification status changed to read.", {}, Meteor.userId())
				} else {
					log.warn("Warning: User not logged in.");
					throw "USER_NOT_LOGGED_IN";
				}
			} catch (error) {
				log.error("Error: In app Notifications status changing failed. | Error: " + error, {}, Meteor.userId());
			}
		}
	}
})