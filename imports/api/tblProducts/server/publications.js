// All tblProductVarities-related publications

import { Meteor } from 'meteor/meteor';
import { tblProducts } from '../tblProducts.js';

// Publishing all the documents inside tblPublish
Meteor.publish('products.all', function () {
	return tblProducts.find();
});