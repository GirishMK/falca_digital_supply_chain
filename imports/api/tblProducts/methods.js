import { tblProducts } from './tblProducts.js';
import { check } from 'meteor/check';
import { Mongo } from 'meteor/mongo';
import { toCamelCase } from '../../api/extras/functions.js';
import { log } from "/imports/startup/both/index.js";
import { Product_States } from '../extras/constants.js';

Meteor.methods({
	// function to add a product in the system
	"create.product": function (product) {
		try {
			log.debug("Creating a product", {}, Meteor.userId());
			check(product, Object); // TODO: Put a proper check on product object
			product.Name = toCamelCase(product.Name);
			product.Category = toCamelCase(product.Category);
			for (unit in product.Units) {
				product.Units[unit] = toCamelCase(product.Units[unit]);
			}
			for (spec in product.Specifications) {
				product.Specifications[spec].Type = toCamelCase(product.Specifications[spec].Type);
				for (val in product.Specifications[spec].Value) {
					product.Specifications[spec].Value[val] = toCamelCase(product.Specifications[spec].Value[val]);
				}
			}
			log.debug("Product Object | Name: " + product.Name + " | Category: " + product.Category + " | Units: " + product.Units.toString() +
				" | Specifications: " + product.Specifications, {}, Meteor.userId());
			product._id = new Mongo.ObjectID();
			product.CreatedAt = new Date();
			product.isActive = true;
			product.Image = "/img/" + (product["Name"].replace(/\s */, "")).toLowerCase() + ".png";

			var reg_prodName = new RegExp(product.Name, 'i');
			if (tblProducts.find({ "Name": reg_prodName }).count() === 0) {
				let insertObject = tblProducts.insert(product);
				if (insertObject) {
					log.info("Success: Product inserted | Name: " + product.Name, {}, Meteor.userId());
				}
				return insertObject;
			} else {
				log.error("Error: Product insertion failed | Reason: Product already exists in database. | Name: " + product.Name, {}, Meteor.userId());
				throw "DUPLICATE_PRODUCT";
			}
		} catch (error) {
			log.error("Error: Product insertion failed | Name: " + product.Name, {}, Meteor.userId());
			throw (error);
		}
	},

	// function to disable a product
	"disable.product": function (productId) {
		try {
			log.debug("Disabling the product | ProductId: " + productId, {}, Meteor.userId());
			check(productId, String);
			let isDisabled = tblProducts.update({
				"_id": new Mongo.ObjectID(productId)
			}, {
					$set: {
						"isAlive": true
					}
				});
			return isDisabled;
		} catch (error) {
			log.error("Error: Disabling product failed | ProductId: " + productId, {}, Meteor.userId());
			throw (error);
		}
	},
	// function to enable a product
	"enable.product": function (productId) {
		try {
			log.debug("Enabling the product | ProductId: " + productId, {}, Meteor.userId());
			check(productId, String);
			let isEnabled = tblProducts.update({
				"_id": new Mongo.ObjectID(productId)
			}, {
					$set: {
						"isAlive": false
					}
				});
			return isEnabled;
		} catch (error) {
			log.error("Error: Enabling product failed | ProductId: " + productId, {}, Meteor.userId());
			throw (error);
		}
	},
	
	// Function to disable the product
	"delete.product": function (productId) {
		try {
			log.debug("Deleting a product | ProductId: " + productId, {}, Meteor.userId());
			check(productId, String);
			let isDeleted = tblProducts.remove({ "_id": new Mongo.ObjectID(productId) });
			if (isDeleted) {
				log.info("Success: Toggled product successfully | ProductId: " + productId, {}, Meteor.userId());
			}
			return isDeleted;
		} catch (error) {
			log.error("Error: Deleting product failed | ProductId: " + productId + " | Error: " + error, {}, Meteor.userId());
			throw (error);
		}
	}
})