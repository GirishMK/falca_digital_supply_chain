import { Meteor } from 'meteor/meteor';
import { tblReleaseVersion } from "../tblReleaseVersion.js";

// Publishing all the documents inside tblPublish
Meteor.publish('ReleaseVersion.all', function () {	
	return tblReleaseVersion.find({});
});