import { Meteor } from 'meteor/meteor';
import { tblAndroidAppstoreAnalytics } from '../tblAndroidAppstoreAnalytics.js';

// Publishing all the documents inside tblPublish
Meteor.publish('appstore.all', function () {
	return tblAndroidAppstoreAnalytics.find();
});