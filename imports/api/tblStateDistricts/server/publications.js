import { Meteor } from 'meteor/meteor';
import { tblStateDistricts } from '../tblStateDistricts.js';

// Publishing all the documents inside tblPublish
Meteor.publish('stateDistricts.all', function () {
	return tblStateDistricts.find();
});