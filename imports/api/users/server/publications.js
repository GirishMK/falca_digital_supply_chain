// All users-related publications

import { Meteor } from 'meteor/meteor';

// Publishing all the users in the database
// Possible dangers: Should not publish all
//                   Should limit the data passing

Meteor.publish("publish.unassigned.consumer.count", function () {
	var user = Meteor.users.findOne(this.userId);
	if(user){
	if (user.profile.Role === "SuperAdmin") {
		Counts.publish(this, "publish.unassigned.consumer.count", Meteor.users.find({
			"profile.Role": "Consumer",
			"profile.UID.IsVerified": false,
			"profile.UID.IsRejected": false
		}), { fastCount: true });
	} else {
		Counts.publish(this, "publish.unassigned.consumer.count", Meteor.users.find({
			"profile.Role": "Consumer",
			"profile.UID.IsVerified": false,
			"profile.UID.IsRejected": false,
			"profile.Address.District": user.profile.Address.District
		}), { fastCount: true });
	}
}
});
Meteor.publish("publish.verified.consumer.count", function () {
	var user = Meteor.users.findOne(this.userId);
	if(user){	
	if (user.profile.Role === "SuperAdmin") {
		Counts.publish(this, "publish.verified.consumer.count", Meteor.users.find({
			"profile.Role": "Consumer",
			"profile.UID.IsVerified": true,
			"profile.UID.IsRejected": false
		}), { fastCount: true });
	} else {
		Counts.publish(this, "publish.verified.consumer.count", Meteor.users.find({
			"profile.Role": "Consumer",
			"profile.UID.IsVerified": true,
			"profile.UID.IsRejected": false,
			"profile.Address.District": user.profile.Address.District
		}), { fastCount: true });
	}
}
});
Meteor.publish('publish.users.profile', function (role) {
	if(role){
	return Meteor.users.find({
		"profile.Role": { $in: role }
	}, {
			fields: {
				_id: 1,
				profile: 1,
				createdAt: 1
			},
			sort: {
				createdAt: -1
			}
		});
	}
});
//publishing users based on id aadhaardetail page
Meteor.publish('publish.users.id', function (role) {
	if(role){
	return Meteor.users.find({
		"_id": role
	});
	}
});

Meteor.publish('publish.users', function () {
	return Meteor.users.find({});
});
//aadhar registered count
Meteor.publish("publish.unassigned.aadhar.count", function () {
	var user = Meteor.users.findOne(this.userId);
	if (user.profile.Role === "SuperAdmin") {
		var unassignAadhaarCount = Meteor.users.find({
			$and: [{
				"profile.Role": "Farmer", "profile.UID.Type": "Aadhaar", "profile.UID.IsVerified": false,
				"profile.UID.VerifiedBy": "", "profile.UID.Value": { $ne: "" }, "profile.Name": { $ne: "" }
			}]
		});
		Counts.publish(this, "publish.unassigned.aadhar.count", unassignAadhaarCount, { fastCount: true });
	} else {
		var unassignAadhaarCount_admin = Meteor.users.find({
			$and: [{
				"profile.Role": "Farmer", "profile.UID.Type": "Aadhaar", "profile.UID.IsVerified": false,
				"profile.UID.VerifiedBy": "", "profile.Address.District": user.profile.Address.District, "profile.UID.Value": { $ne: "" }, "profile.Name": { $ne: "" }
			}]
		});
		Counts.publish(this, "publish.unassigned.aadhar.count", unassignAadhaarCount_admin, { fastCount: true });
	}
});
Meteor.publish("publish.unassigned.aadharimage.count", function () {
	var unassignImageCount = Meteor.users.find({
		$and: [{
			"profile.Role": "Farmer", "profile.UID.Type": "Aadhaar", "profile.UID.IsVerified": false,
			"profile.UID.VerifiedBy": "", "profile.UID.aadharImageUploadId": { $ne: "" }
		}]
	});
	Counts.publish(this, "publish.unassigned.aadharimage.count", unassignImageCount, { fastCount: true });
});
Meteor.publish("publish.assigned.aadhar.count", function () {
	var user = Meteor.users.findOne(this.userId);
	if (user.profile.Role === "SuperAdmin") {
		var assignAadhaarCount = Meteor.users.find({
			$and: [{
				"profile.Role": "Farmer", "profile.UID.Type": "Aadhaar", "profile.UID.IsVerified": false,
				"profile.UID.VerifiedBy": { $ne: "" }, "profile.UID.Value": { $ne: "" }
			}]
		});
		Counts.publish(this, "publish.assigned.aadhar.count", assignAadhaarCount, { fastCount: true });
	} else {
		var assignAadhaarCount_admin = Meteor.users.find({
			$and: [{
				"profile.Role": "Farmer", "profile.UID.Type": "Aadhaar", "profile.UID.IsVerified": false,
				"profile.UID.VerifiedBy": { $ne: "" }, "profile.Address.District": user.profile.Address.District,
				"profile.UID.Value": { $ne: "" }
			}]
		});
		Counts.publish(this, "publish.assigned.aadhar.count", assignAadhaarCount_admin, { fastCount: true });
	}
});
Meteor.publish("publish.assigned.aadharimage.count", function () {
	var assignImageCount = Meteor.users.find({
		$and: [{
			"profile.Role": "Farmer", "profile.UID.Type": "Aadhaar", "profile.UID.IsVerified": false,
			"profile.UID.VerifiedBy": { $ne: "" }, "profile.UID.aadharImageUploadId": { $ne: "" }
		}]
	});
	Counts.publish(this, "publish.assigned.aadharimage.count", assignImageCount, { fastCount: true });
});

