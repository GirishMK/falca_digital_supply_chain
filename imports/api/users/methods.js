import { check } from 'meteor/check';
import '../extras/methods.js';
import { log } from "/imports/startup/both/index.js";
import { Accounts } from 'meteor/accounts-base';
var shortid = require('shortid');

shortid.generateReferralCode = function (name) {
    try {
        if (name) {
            var nameStartwith = name.slice(0, 4);
            shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');
            return (((nameStartwith ? nameStartwith : "") + shortid.generate().slice(0, 5)).toLowerCase());
        }
    } catch (e) {
        log.info("Error: while generating referral code | Error: " + e);
    }
}

Meteor.methods({
    // Function to delete a user by userid
    "users.delete": function (userId) {
        try {
            log.debug("Deleting user | UserId: " + userId, {}, Meteor.userId());
            check(userId, String);
            log.info("Success: User deleted | UserId: " + userId, {}, Meteor.userId());
            return Meteor.users.remove({ "_id": userId });
        } catch (error) {
            log.error("Error: User deletion failed | UserId: " + userId + " | Error: " + error, {}, Meteor.userId());
            throw error;
        }
    },

    //Function to reject a Farmer by userId
    'users.rejectFarmer': function (farmerid) {
        try {
            log.debug("Aadhaar rejection started. | FarmerId: " + farmerid, {}, Meteor.userId());
            check(farmerid, String);
            if (farmerid !== "undefined") {
                var farmer = Meteor.users.find(
                    {
                        "_id": farmerid
                    }).fetch()[0];
                    var aadharImageUploaded = farmer.profile.UID.aadharImageUploaded;
                    var value = farmer.profile.UID.Value;
                if (farmer) {
                  let isUpdated = Meteor.users.update({
                        "_id": farmerid
                    }, {
                            $set: {
                                "profile.UID.Value": "",
                                "profile.UID.IsVerified": false,
                                "profile.UID.VerifiedBy": "",
                                "profile.UID.VerifiedByName": "",
                                "profile.UID.VerifiedByPhone": "",
                                "profile.UID.aadharImageUploaded":false,
                                "profile.UID.aadharImageUploadId":"",
                                "profile.IsAlive": false,
                            }
                        });
                        if (isUpdated) {
                            log.info("Success: User updated | UserId: " + farmerid, {}, Meteor.userId());
                            Meteor.call("fetch.gcmId.farmer", farmerid, function (error, gcmidarray) {
                                if (error) {
                                    Meteor.call("Fetching gcmids of farmer failed: " + error);
                                    throw error;
                                } else {
                                    if (gcmidarray) {                                
                                        if(aadharImageUploaded === true || value === "MANUAL") {                        
                                            var title = "Address-Proof Rejected.";
                                            var body = "Your Address-Proof has been rejected. Please link valid Address-Proof";
                                            var type = "Aadhar_Rejection";
                                            var url= {
                                                page: "/home", 
                                                farmerid: farmerid,
                                            }    
                                        }
                                            else{
                                            var title = "Address Proof Rejected.";
                                            var body = "Your address-Proof has been rejected. Please link valid address-Proof";
                                            var type = "Aadhar_Rejection";
                                            var url= {
                                                page: "/home"
                                            } 
                                        }   
                                        Meteor.call("send.push.notification", gcmidarray, title, body, farmerid, type, url);
                                    }
                                }
                            })
                            return isUpdated;
                        } else {
                            log.error("Error: Updating user failed | Reason: " + isUpdated + " | UserId: " + farmerid, {}, Meteor.userId());
                            throw isUpdated;
                        }
                } else {
                    log.warn("Warning: Aadhaar rejection failed | Reason: No farmer exists with the given id. | FarmerId: " + farmerid, {}, Meteor.userId());
                    throw "NO_FARMER_WITH_GIVEN_ID";
                }
            } else {
                log.warn("Warning: Aadhaar rejection failed | Reason: One of the parameters is wrong. | FarmerId: " + farmerid, {}, Meteor.userId());
                throw "PARAMETERS_TO_THE_FUNCTION_WRONG";
            }
        } catch (error) {
            log.error("Error: Aahaar rejection failed. | Error: " + error + " | FarmerId: " + farmerid, {}, Meteor.userId());
            throw error;
        }
    },
    "assignAddress.verify": function (farmerId) {
        try {
            log.debug("Verifying addresss of login type FPO | FarmerId: " + farmerId + " | FTMId: ", {}, Meteor.userId());
            check(farmerId, String);
            var updateSuccess = Meteor.users.update({
                "_id": farmerId,
            }, {
                $set: {
                    "profile.UID.VerifiedBy": Meteor.userId(),
                    "profile.UID.VerifiedByName": Meteor.user().profile.Name,
                    "profile.UID.VerifiedByPhone": Meteor.user().profile.Phone.Primary,
                    "profile.UID.AssignedBy": Meteor.userId(),
                    "profile.UID.IsVerified":true
                }
            });
            return updateSuccess;
        }
        catch (error) {
            log.error("Error: Cancellation of Verifying addresss of login type FPO | FarmerId: " + farmerId + " | Error: " + error, {}, Meteor.userId());
            throw (error);
        }
    },

    // Funtion to assign an aadhar to a field team
    "assignAadhar.toFieldTeam": function (farmerId, backendId) {
        try {
            log.debug("Assigning aadhaar to FTM | FarmerId: " + farmerId + " | FTMId: " + backendId, {}, Meteor.userId());
            check(farmerId, String);
            check(backendId, String);
            var backendUser = (Meteor.users.find({ "_id": backendId }).fetch())[0];

            if (backendUser) {
                var updateSuccess = Meteor.users.update({
                    "_id": farmerId,
                }, {
                        $set: {
                            "profile.UID.VerifiedBy": backendId,
                            "profile.UID.VerifiedByName": backendUser.profile.Name,
                            "profile.UID.VerifiedByPhone": backendUser.profile.Phone.Primary,
                            "profile.UID.AssignedBy": Meteor.userId()
                        }
                    });
                var farmer = (Meteor.users.find({ "_id": farmerId }).fetch())[0];
                if (updateSuccess && farmer) {
                    log.info("Success: Assigned aadhaar to FTM  | Stage: Aadhaar Updated | FarmerId: " + farmerId + " | FTMId: " + backendId, {}, Meteor.userId());
                    log.debug("Sending push notification");
                    Meteor.call("fetch.gcmId.falcaperson", backendId, function (error, gcmidarray) {
                        if (error) {
                            Meteor.call("Fetching gcmids of falca team member failed: " + error);
                            throw error;
                        } else {
                            if (gcmidarray) {     
                                if(farmer.profile.UID.aadharImageUploaded === true) {                        
                                var title = "New Address-Proof assigned for approval.";
                                var body = "A new address-proof has been assigned to you for verification.";
                                var type = "Aadhar_Assignment";
                                var url= {
                                    page: "/aadhaarApproval",
                                    farmerid: farmerId,
                                    farmerUidValue:farmer.profile.UID.Value
                                }    
                            }
                            else if(farmer.profile.UID.Value === "MANUAL")
                            {
                                var title = "New Address-Proof assigned for approval.";
                                var body = "A new address-proof has been assigned to you for verification." + "Farmer Name: " + farmer.profile.Name +" "+ "District: " + farmer.profile.Address.District + ".";
                                var type = "Aadhar_Assignment";
                                var url= {
                                    page: "/aadhaarApproval",
                                    farmerid: farmerId,
                                    farmerUidValue:farmer.profile.UID.Value
                                }
                            }
                                else{
                                var title = "New address proof assigned for approval.";
                                var body = "A new address proof has been assigned to you for verification." + "Farmer Name: " + farmer.profile.Name +" "+ "District: " + farmer.profile.Address.District + ".";
                                var type = "Aadhar_Assignment";
                                var url= {
                                    page: "/aadhaarApproval",
                                    farmerid: farmerId,
                                    farmerUidValue:farmer.profile.UID.Value
                                } 
                            }                                                                                      
                                Meteor.call("send.push.notification", gcmidarray, title, body, farmerId, type, url);
                            }
                        }
                    })
                    log.info("Success: Assigned aadhaar to FTM  | Stage: Final | FarmerId: " + farmerId + " | FTMId: " + backendId, {}, Meteor.userId());
                    return updateSuccess;
                } else {
                    log.error("Error: Assigning aadhaar to FTM failed | Reason: " + updateSuccess + " " + farmer + " | FarmerId: " + farmerId + " | FTMId: " + backendId, {}, Meteor.userId());
                    throw updateSuccess;
                }
            } else {
                log.error("Error: Assigning aadhaar to FTM failed | Reason: No FTM exists with given id | FarmerId: " + farmerId + " | FTMId: " + backendId, {}, Meteor.userId());
                throw "NO_FTM_WITH_USER_ID";
            }
        } catch (error) {
            log.error("Error: Assigning aadhaar to FTM failed | FarmerId: " + farmerId + " | FTMId: " + backendId + " | Error: " + error, {}, Meteor.userId());
            throw (error);
        }
    },

    // Funtion to cancel an assignment of aadhar to a field team
    "cancelAssignment.Aadhar": function (farmerId, assignTo) {
        try {
            log.debug("Cancelling aadhar assignment | FarmerId: " + farmerId, {}, Meteor.userId());
            check(farmerId, String);
            check(assignTo, String);
            if (farmerId) {
                try {
                    Meteor.call("fetch.gcmId.falcaperson", assignTo, function (error, gcmidarray) {
                        if (error) {
                            Meteor.call("Fetching gcmids of falca team member failed: " + error);
                            throw error;
                        } else {
                            if (gcmidarray) {    
                                var farmer = Meteor.users.find({"_id": farmerId}).fetch()[0];         
                                if(farmer.profile.UID.aadharImageUploaded === true || farmer.profile.UID.Value === "MANUAL") {
                                    var title = "Address-Proof Unassigned";
                                    var body = "Address-Proof for Verification has been removed from your assignment list";
                                    var type = "Aadhar_UnAssignment";
                                    var url= {
                                        page: "/aadhaarApproval",
                                        farmerid: farmerId,
                                        farmerUidValue:farmer.profile.UID.Value
                                    }
                                }          
                                else{
                                    var title = "Address Proof Unassigned";
                                    var body = "Address proof has been removed from your assignment list";
                                    var type = "Aadhar_UnAssignment";
                                    var url= {
                                        page: "/aadhaarApproval",
                                        farmerid: farmerId,
                                        farmerUidValue:farmer.profile.UID.Value
                                    }
                                }  
                                Meteor.call("send.push.notification", gcmidarray, title, body, farmerId, type, url);
                          }
                        }
                    })
                    return Meteor.users.update({
                            "_id": farmerId,
                        }, {
                                $set: {
                                    "profile.UID.VerifiedBy": "",
                                    "profile.UID.VerifiedByName": "",
                                    "profile.UID.VerifiedByPhone": "",
                                }
                            });
                }
                catch (error) {
                    log.error("Error:Cancelling the assignment of perishable transport load details failed. | Error: " + error, "| TransportId:" + transportId, {}, Meteor.userId());
                    throw error;
                }
            }
            // let isCancelled = Meteor.users.update({
            //     "_id": farmerId,
            // }, {
            //         $set: {
            //             "profile.UID.VerifiedBy": "",
            //             "profile.UID.VerifiedByName": "",
            //             "profile.UID.VerifiedByPhone": "",
            //         }
            //     });
            // if (isCancelled) {
                // var farmer = Meteor.users.find({"_id": farmerId}).fetch()[0];
                // var title = "Aadhaar Unassigned";
                // var body = "Aadhaar(" + farmer.profile.UID.Value + ") has been removed from your assignment list";
                // var type = "Aadhar_UnAssignment";
                // generatePushNotification(farmerId, assignTo, title, body, type);

            //     log.info("Success: Aadhaar assignment cancelled | FarmerId: " + farmerId, {}, Meteor.userId());
            //     return isCancelled;
            // } else {
            //     log.error("Error: Cancellation of assigned aadhaar failed | Reason: " + isCancelled + " | FarmerId: " + farmerId, {}, Meteor.userId());
            //     throw isCancelled;
            // }
        } catch (error) {
            log.error("Error: Cancellation of assigned aadhaar failed | FarmerId: " + farmerId + " | Error: " + error, {}, Meteor.userId());
            throw (error);
        }
    },

    // Function to create a user
    "user.create": function (email, profile) {
        try {
            log.debug("Creating a new user | Email: " + email, {}, Meteor.userId());
            check(email, String);
            check(profile, Object); //TODO: Put proper check on profile object          
                var OTP = parseInt(Math.random() * 900000 + 100000).toString();
                if(profile.Role != "Admin"){
                profile.ReferralCode = shortid.generateReferralCode(profile.Name);
                }
            var newUserId = Accounts.createUser({
                email: email,
                profile: profile,
                password: OTP.toString()
            });
            // TODO: Check what will return if user creation fails
            if (typeof (newUserId) === "string") {
                // TODO: Send email with apk link and the id password
                log.info("Success: User created | Email: " + email + " | UserId: " + newUserId, {}, Meteor.userId());
                if(profile.Role === "SalesPerson"){
                    return Meteor.call("send.salesregistration.email", email, OTP.toString());
                }
               else if(profile.Role === "Falcaperson"){
                    return Meteor.call("send.registration.email", email, OTP.toString());    
                }
                else if(profile.Role === "StoreManager"){
                    return Meteor.call("send.storeregistration.email", email, OTP.toString());    
                }
               else if(profile.Role === "Admin"){
                    return Meteor.call("send.adminregistration.email", email, OTP.toString());    
                }
            }
        }
        catch (error) {
            log.error("Creating user failed | Email: " + email + " | Error: " + error, {}, Meteor.userId());
            throw (error);
        }
    },
     //function to update farmer details when he is registered with only phone
     "farmer.profile.update": function (id, profile) {
        try {
            log.debug("Updating user | UserId: " + id, {}, Meteor.userId());
            check(id, String);
            check(profile, Object); //TODO: Put proper check on profile object
            var verifiedBy = Meteor.users.find({ "_id": Meteor.userId() }).fetch()[0];
            let isUpdated = Meteor.users.update({
                "_id": id
            }, {
                    $set: {
                        "profile.Name": profile.Name,
                        "profile.Address": profile.Address,
                        "profile.UID.Value": "MANUAL",
                        "profile.UID.IsVerified": true,
                        "profile.UID.VerifiedBy" : Meteor.userId(),
                        "profile.UID.VerifiedByName":  verifiedBy.profile.Name,
                        "profile.UID.VerifiedByPhone": verifiedBy.profile.Phone.Primary                       
                    }
                })
            if (isUpdated) {
                log.info("Success: User updated | UserId: " + id, {}, Meteor.userId());
                Meteor.call("fetch.gcmId.farmer", id, function (error, gcmidarray) {
                    if (error) {
                        Meteor.call("Fetching gcmids of farmer failed: " + error);
                        throw error;
                    } else {
                        if (gcmidarray) {                                
                                var title = "Address Details Verified.";
                                var body = "Hi " + profile.Name + "!! Your Address has been verified successfully!!!";
                                var type = "Aadhar_Verified";
                                var url= {
                                    page: "/home"
                                }    
                            Meteor.call("send.push.notification", gcmidarray, title, body, id, type, url);
                        }
                    }
                })
                return isUpdated;
            } else {
                log.error("Error: Updating user failed | Reason: " + isUpdated + " | UserId: " + id, {}, Meteor.userId());
                throw isUpdated;
            }
        } catch (error) {
            log.error("Error: Updating user failed | UserId: " + id + " | Error: " + error, {}, Meteor.userId());
        }
    },

    // Function to update users details
    "user.update": function (id, profile) {
        try {
            log.debug("Updating user | UserId: " + id, {}, Meteor.userId());
            check(id, String);
            check(profile, Object); //TODO: Put proper check on profile object
            var user = Meteor.users.find({ "_id": id }).fetch()[0];
            if(user.profile.Role === "Admin"){
                var isUpdated = Meteor.users.update({
                    "_id": id
                }, {
                        $set: {
                            "profile.Name": profile.Name,
                            "profile.Phone.Primary": profile.Phone.Primary,
                            "profile.Address": profile.Address,
                            "profile.Organisation": profile.Organisation
                        }
                    })  
            }
            else{
            var isUpdated = Meteor.users.update({
                "_id": id
            }, {
                    $set: {
                        "profile.Name": profile.Name,
                        "profile.Phone.Primary": profile.Phone.Primary,
                        "profile.Address": profile.Address
                    }
                })
            }
            if (isUpdated) {
                log.info("Success: User updated | UserId: " + id, {}, Meteor.userId());
                return isUpdated;
            } else {
                log.error("Error: Updating user failed | Reason: " + isUpdated + " | UserId: " + id, {}, Meteor.userId());
                throw isUpdated;
            }
        } catch (error) {
            log.error("Error: Updating user failed | UserId: " + id + " | Error: " + error, {}, Meteor.userId());
        }
    },

    //Function to verify the consumer
    "consumer.verification": function (consumerid, reason) {
        try {
            log.debug("Verifying user | ConsumerId: " + consumerid, {}, Meteor.userId());
            check(consumerid, String);
            check(reason, String);
            let isVerified = Meteor.users.update({
                "_id": consumerid
            }, {
                    $set: {
                        "profile.UID.IsVerified": true,
                        "profile.UID.Reason": reason,
                    }
                })
            if (isVerified) {
                log.info("Success: User verified | ConsumerId: " + consumerid, {}, Meteor.userId());
            } else {
                log.error("Error: Verifying user failed | Reason: " + isVerified + " | ConsumerId: " + consumerid, {}, Meteor.userId());
                throw isVerified;
            }
        }
        catch (error) {
            log.error("Error: Verifying user failed | ConsumerId: " + consumerid + " | Error: " + error, {}, Meteor.userId());
            console.log(error);
        }
    },

    "consumer.rejection": function (consumerid, reason) {
        try {
            log.debug("Rejecting user | ConsumerId: " + consumerid, {}, Meteor.userId());
            check(consumerid, String);
            check(reason, String);
            let isRejected = Meteor.users.update({
                "_id": consumerid
            }, {
                    $set: {
                        "profile.UID.IsRejected": true,
                        "profile.UID.Reason": reason,
                    }
                })
            if (isRejected) {
                log.info("Success: User rejected | ConsumerId: " + consumerid, {}, Meteor.userId());
            } else {
                log.error("Error: Rejecting user failed | Reason: " + isRejected + " | ConsumerId: " + consumerid, {}, Meteor.userId());
                throw isRejected;
            }
        }
        catch (e) {
            log.error("Error: Rejecting user failed | ConsumerId: " + consumerid + " | Error: " + error, {}, Meteor.userId());
            console.log(error);
        }
    },

    // Function to fetch GCM ids of falcapersons
    "fetch.gcmId.falcaperson": function (id = undefined) {
        try {
            log.debug("Fetching FTM gcmids | id: " + id, {}, Meteor.userId());
            var queryObject = {};
            if (id) queryObject._id = id;
            queryObject["profile.Role"] = "Falcaperson";
            if (id) {
                let gcmids = Meteor.users.find(queryObject).fetch()[0].profile.GCMID;
                log.info("Success: GCMId fethced | id: " + id + " | GCMId: " + gcmids.toString(), {}, Meteor.userId());
                return [gcmids];
            } else {
                var backend = Meteor.users.find(queryObject).fetch()
                    .filter(function (a) {
                        if (typeof a.profile.GCMID !== "undefined") {
                            return a.profile.GCMID;
                        }
                    });
                if (backend && backend.length > 0) {
                    var backendDevicesIds = [];
                    for (var i in backend) {
                        var current = backend[i].profile.GCMID;
                        if (typeof current !== "undefined" && current.length > 0) {
                            backendDevicesIds.push(current);
                        }
                    }
                    log.info("Success: GCMId fethced | id: " + id + " | GCMId: " + backendDevicesIds.toString(), {}, Meteor.userId());
                    return backendDevicesIds;
                }
                log.warn("Warning: No GCMId fethced | id: " + id, {}, Meteor.userId());
                return [];
            }
        } catch (error) {
            log.error("Error: Fetching gcmids failed | id: " + id + " | Error: " + error, {}, Meteor.userId());
            return [];
        }
    },
    // Function to fetch GCM ids of salesperson
    "fetch.gcmId.salesperson": function (id = undefined) {
        try {
            log.debug("Fetching Salesperson gcmids | id: " + id, {}, Meteor.userId());
            var queryObject = {};
            if (id) queryObject._id = id;
            queryObject["profile.Role"] = "SalesPerson";
            if (id) {
                let gcmids = Meteor.users.find(queryObject).fetch()[0].profile.GCMID;
                log.info("Success: GCMId fethced | id: " + id + " | GCMId: " + gcmids.toString(), {}, Meteor.userId());
                return [gcmids];
            } else {
                var backend = Meteor.users.find(queryObject).fetch()
                    .filter(function (a) {
                        if (typeof a.profile.GCMID !== "undefined") {
                            return a.profile.GCMID;
                        }
                    });
                if (backend && backend.length > 0) {
                    var backendDevicesIds = [];
                    for (var i in backend) {
                        var current = backend[i].profile.GCMID;
                        if (typeof current !== "undefined" && current.length > 0) {
                            backendDevicesIds.push(current);
                        }
                    }
                    log.info("Success: GCMId fethced | id: " + id + " | GCMId: " + backendDevicesIds.toString(), {}, Meteor.userId());
                    return backendDevicesIds;
                }
                log.warn("Warning: No GCMId fethced | id: " + id, {}, Meteor.userId());
                return [];
            }
        } catch (error) {
            log.error("Error: Fetching gcmids failed | id: " + id + " | Error: " + error, {}, Meteor.userId());
            return [];
        }
    },
    "fetch.gcmId.farmer": function (id = undefined) {
        try {
            log.debug("Fetching Farmer gcmids | id: " + id, {}, Meteor.userId());
            var queryObject = {};
            if (id) queryObject._id = id;
            queryObject["profile.Role"] = "Farmer";
            if (id) {
                let gcmids = Meteor.users.find(queryObject).fetch()[0].profile.GCMID;
                log.info("Success: GCMId fethced | id: " + id + " | GCMId: " + gcmids.toString(), {}, Meteor.userId());
                return [gcmids];
            } else {
                var backend = Meteor.users.find(queryObject).fetch()
                    .filter(function (a) {
                        if (typeof a.profile.GCMID !== "undefined") {
                            return a.profile.GCMID;
                        }
                    });
                if (backend && backend.length > 0) {
                    var backendDevicesIds = [];
                    for (var i in backend) {
                        var current = backend[i].profile.GCMID;
                        if (typeof current !== "undefined" && current.length > 0) {
                            backendDevicesIds.push(current);
                        }
                    }
                    log.info("Success: GCMId fethced | id: " + id + " | GCMId: " + backendDevicesIds.toString(), {}, Meteor.userId());
                    return backendDevicesIds;
                }
                log.warn("Warning: No GCMId fethced | id: " + id, {}, Meteor.userId());
                return [];
            }
        } catch (error) {
            log.error("Error: Fetching gcmids failed | id: " + id + " | Error: " + error, {}, Meteor.userId());
            return [];
        }
    },
    "user.resetPassword": function (fieldUserId) {
        try {
            log.debug("Updating password for user | FieldUserId: " + fieldUserId, {}, Meteor.userId());
            check(fieldUserId, String);
            var fieldUser = (Meteor.users.find({ "_id": fieldUserId }).fetch())[0];
            var newPassword = parseInt(Math.random() * 900000 + 100000).toString();
            Accounts.setPassword(fieldUserId, newPassword,{ logout: true });
            return newPassword;
        }
        catch (error) {
            log.error("Error: While resetting password | FieldUserId: " + fieldUserId + " | Error: " + error, {}, Meteor.userId());
        }
    },
     // Function to create a user
     "admin.user.create": function (email, profile) {
        try {           
            log.debug("Creating a new admin user | Email: " + email, {}, Meteor.userId());
            check(email, String);
            check(profile, Object); //TODO: Put proper check on profile object
            var OTP = parseInt(Math.random() * 900000 + 100000).toString();           
            var newUserId = Accounts.createUser({
                email: email,
                profile: profile,
                password: OTP.toString()
            });
            // TODO: Check what will return if user creation fails
            if (typeof (newUserId) === "string") {
                // TODO: Send email and the id password
                log.info("Success:Admin User created | Email: " + email + " | UserId: " + newUserId, {}, Meteor.userId());
                return Meteor.call("send.adminregistration.email", email, OTP.toString());
            }
        }
        catch (error) {
            log.error("Creating adminuser failed | Email: " + email + " | Error: " + error, {}, Meteor.userId());
            throw (error);
        }
    },
    'AddGCMID': function (gcmid) {
        try {
            log.debug("Adding gcmid | GCMId: " + gcmid, {}, Meteor.userId());
            if (typeof gcmid === "string" && gcmid) {
                Meteor.users.update({ "_id": Meteor.userId() }, {
                    $set:
                        {
                            "profile.GCMID": gcmid
                        }
                });
                log.info("Success: GCM id added | GCMId: " + gcmid, {}, Meteor.userId());
            } else {
                log.warn("Warning: Adding gcmid failed. | Reason: GCM id is incorrect | GCMId: " + gcmid, {}, Meteor.userId());
                throw "INCORRECT_PARAMETERS_TO_FUNCTION";
            }
        } catch (error) {
            log.error("Error: Adding GCM Id failed. | GCMId: " + gcmid + " | Error: " + error, {}, Meteor.userId());
            throw error;
        }
    },
    'UpdateProfile': function (updateprofile) {
        try {
            if (Meteor.userId()) {
                log.debug("Profile updation initiated", {}, Meteor.userId());
                var profileedit = Meteor.users.find({ "_id": Meteor.userId() }).fetch()[0];
                if (profileedit) {
                    Meteor.users.update(
                        { "_id": Meteor.userId() },
                        {
                            $set: {
                                "profile.Phone.Primary": updateprofile.Phone,
                            }
                        });
                    log.info("Success: Profile updated", {}, Meteor.userId());
                } else {
                    log.warn("Warning: Profile updation failed. | Reason: No user with the given id.", {}, Meteor.userId());
                    throw "INCORRECT_ID";
                }
            }
            else {
                log.warn("Warning: Profile updation failed. | Reason: User not logged in.", {}, Meteor.userId());
                throw "USER_NOT_LOGGED_IN";
            }
        } catch (error) {
            log.error("Error: Profile updation failed. | Error: " + error, {}, Meteor.userId());
            throw error;
        }
    }
})