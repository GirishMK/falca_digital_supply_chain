// All tblPublish-related publications

import {
	Meteor
} from 'meteor/meteor';
import {
	tblPublish
} from '../tblPublish.js';

import {
	Product_States,
	Perishable_States,
	MAXIMUM_PRODUCTS_LIMIT
} from "/imports/api/extras/constants.js";

Meteor.publish("publish.productCancel.count", function () {
	var user = Meteor.users.findOne(this.userId);
	if (user) {
		Counts.publish(this, "publish.productCancel.count", tblPublish.find({
			"ProductDetails.Location.District": Meteor.user().profile.Address.District,
			"FarmerInfo.Name": "FalcaFarmer",
			"Status": Product_States[3]
		}), {
			fastCount: true
		});

	}
});

Meteor.publish('publish.product.id', function (prodId) {
	if (prodId) {
		return tblPublish.find({
			"_id": prodId
		}, {
			fields: {
				_id: 1,
				ProductId: 1,
				FarmerId: 1,
				FarmerInfo: 1,
				ApprovedBy: 1,
				FinalizedBy: 1,
				ProductDetails: 1,
				Status: 1,
				Perishable: 1,
				createdAt: 1,
				PaymentRecieved: 1,
				OrderDetails: 1,
				PaymentFromConsumer: 1,
				TransportDetails: 1,
				ShipProductTo: 1,
				TransportFrom: 1,
				PostedBy: 1
			},
			sort: {
				"createdAt": -1
			}
		})
	}
});
Meteor.publish('publish.nonperishable', function (status) {
	if (status) {
		return tblPublish.find({
			Status: {
				$in: status
			},
			Perishable: "No"
		}, {
			fields: {
				_id: 1,
				ProductId: 1,
				FarmerInfo: 1,
				FarmerId: 1,
				ApprovedBy: 1,
				ProductDetails: 1,
				OrderDetails: 1,
				Perishable: 1,
				Status: 1,
				FinalizedBy: 1,
				TotalBids: 1,
				BidDetails: 1,
				BidAcceptedDate: 1,
				UnloadStage: 1,
				TransportDetails: 1,
				BidDetails: 1,
				createdAt: 1,
				ShipProductTo: 1,
				TransportFrom: 1,
				PostedBy: 1
			},
			sort: {
				"createdAt": -1
			}
		})
	}
})
Meteor.publish('publish.perishable', function (status) {
	if (status) {
		return tblPublish.find({
			Status: {
				$in: status
			},
			Perishable: "Yes"
		}, {
			fields: {
				_id: 1,
				ProductId: 1,
				FarmerInfo: 1,
				FarmerId: 1,
				ApprovedBy: 1,
				ProductDetails: 1,
				OrderDetails: 1,
				Perishable: 1,
				Status: 1,
				FinalizedBy: 1,
				BidAcceptedDate: 1,
				UnloadStage: 1,
				TransportDetails: 1,
				BidDetails: 1,
				createdAt: 1,
				ShipProductTo: 1,
				TransportFrom: 1,
				PostedBy: 1
			},
			sort: {
				"createdAt": -1
			}
		})
	}
});

//product count
Meteor.publish("publish.unassigned.nonperishable.count", function () {
	var user = Meteor.users.findOne(this.userId);
	if (user) {
		if (user.profile.Role === "SuperAdmin") {
			Counts.publish(this, "publish.unassigned.nonperishable.count", tblPublish.find({
				Status: Product_States[1],
				Perishable: "No"
			}), {
				fastCount: true
			});
		} else {
			Counts.publish(this, "publish.unassigned.nonperishable.count", tblPublish.find({
				"Status": Product_States[1],
				"Perishable": "No",
				"ProductDetails.Location.District": user.profile.Address.District
			}), {
				fastCount: true
			});
		}
	}
});
Meteor.publish("publish.unassigned.perishable.count", function () {
	var user = Meteor.users.findOne(this.userId);
	if (user) {
		if (user.profile.Role === "SuperAdmin") {
			Counts.publish(this, "publish.unassigned.perishable.count", tblPublish.find({
				Status: Product_States[1],
				Perishable: "Yes"
			}), {
				fastCount: true
			});
		} else {
			Counts.publish(this, "publish.unassigned.perishable.count", tblPublish.find({
				"Status": Product_States[1],
				"Perishable": "Yes",
				"ProductDetails.Location.District": user.profile.Address.District
			}), {
				fastCount: true
			});
		}
	}
});

Meteor.publish("publish.assigned.nonperishable.count", function () {
	var user = Meteor.users.findOne(this.userId);
	if (user) {
		if (user.profile.Role === "SuperAdmin") {
			Counts.publish(this, "publish.assigned.nonperishable.count", tblPublish.find({
				Status: Product_States[2],
				Perishable: "No"
			}), {
				fastCount: true
			});
		} else {
			Counts.publish(this, "publish.assigned.nonperishable.count", tblPublish.find({
				"Status": Product_States[2],
				"Perishable": "No",
				"ProductDetails.Location.District": user.profile.Address.District
			}), {
				fastCount: true
			});
		}
	}
});
Meteor.publish("publish.assigned.perishable.count", function () {
	var user = Meteor.users.findOne(this.userId);
	if (user) {
		if (user.profile.Role === "SuperAdmin") {
			Counts.publish(this, "publish.assigned.perishable.count", tblPublish.find({
				Status: Product_States[2],
				Perishable: "Yes"
			}), {
				fastCount: true
			});
		} else {
			Counts.publish(this, "publish.assigned.perishable.count", tblPublish.find({
				"Status": Product_States[2],
				"Perishable": "Yes",
				"ProductDetails.Location.District": user.profile.Address.District
			}), {
				fastCount: true
			});
		}
	}
});

//load count
Meteor.publish("publish.unassigned.nonperishable.load.count", function () {
	var user = Meteor.users.findOne(this.userId);
	if (user) {
		if (user.profile.Role === "SuperAdmin") {
			var nonPerishableBidAccepted = tblPublish.find({
				"Status": Product_States[4],
				"Perishable": "No"
			});
			Counts.publish(this, "publish.unassigned.nonperishable.load.count", nonPerishableBidAccepted, {
				fastCount: true
			});
		} else {
			var nonPerishableBidAccepted = tblPublish.find({
				"Status": Product_States[4],
				"Perishable": "No",
				"ProductDetails.Location.District": user.profile.Address.District
			});
			Counts.publish(this, "publish.unassigned.nonperishable.load.count", nonPerishableBidAccepted, {
				fastCount: true
			});
		}
	}
});
Meteor.publish("publish.unassigned.perishable.load.count", function () {
	var user = Meteor.users.findOne(this.userId);
	if (user) {
		if (user.profile.Role === "SuperAdmin") {
			var perishableBidAccepted = tblPublish.find({
				"Status": {
					$in: [Product_States[3], Product_States[5]]
				},
				"Perishable": "Yes",
				"FinalizedBy": {
					$elemMatch: {
						"id": ""
					}
				}
			});
			Counts.publish(this, "publish.unassigned.perishable.load.count", perishableBidAccepted, {
				fastCount: true
			});
		} else {
			var perishableBidAccepted = tblPublish.find({
				"Status": {
					$in: [Product_States[3], Product_States[5]]
				},
				"Perishable": "Yes",
				"FinalizedBy": {
					$elemMatch: {
						"id": ""
					}
				},
				"ProductDetails.Location.District": user.profile.Address.District
			});
			Counts.publish(this, "publish.unassigned.perishable.load.count", perishableBidAccepted, {
				fastCount: true
			});
		}
	}
});
Meteor.publish("publish.assigned.nonperishable.load.count", function () {
	var user = Meteor.users.findOne(this.userId);
	if (user) {
		if (user.profile.Role === "SuperAdmin") {
			var nonPerishableProduct = tblPublish.find({
				"Status": Product_States[5],
				"Perishable": "No"
			});
			Counts.publish(this, "publish.assigned.nonperishable.load.count", nonPerishableProduct, {
				fastCount: true
			});
		} else {
			var nonPerishableProduct = tblPublish.find({
				"Status": Product_States[5],
				"Perishable": "No",
				"ProductDetails.Location.District": user.profile.Address.District
			});
			Counts.publish(this, "publish.assigned.nonperishable.load.count", nonPerishableProduct, {
				fastCount: true
			});
		}
	}
});
Meteor.publish("publish.assigned.perishable.load.count", function () {
	var user = Meteor.users.findOne(this.userId);
	if (user) {
		if (user.profile.Role === "SuperAdmin") {
			var perishableProduct = tblPublish.find({
				"Status": {
					$in: [Product_States[3], Product_States[5]]
				},
				"Perishable": "Yes",
				"FinalizedBy": {
					$elemMatch: {
						"id": {
							$ne: ""
						}
					}
				}
			});
			Counts.publish(this, "publish.assigned.perishable.load.count", perishableProduct, {
				fastCount: true
			});
		} else {
			var perishableProduct = tblPublish.find({
				"Status": {
					$in: [Product_States[3], Product_States[5]]
				},
				"Perishable": "Yes",
				"ProductDetails.Location.District": user.profile.Address.District,
				"FinalizedBy": {
					$elemMatch: {
						"id": {
							$ne: ""
						}
					}
				}
			});
			Counts.publish(this, "publish.assigned.perishable.load.count", perishableProduct, {
				fastCount: true
			});
		}
	}
});

//unload count
Meteor.publish("publish.unassigned.nonperishable.unload.count", function () {
	var user = Meteor.users.findOne(this.userId);
	if (user) {
		if (user.profile.Role === "SuperAdmin") {
			var perishablecount = tblPublish.find({
				"Status": {
					$in: [Product_States[6], Product_States[10]]
				},
				"Perishable": "No"
			});
			Counts.publish(this, "publish.unassigned.nonperishable.unload.count", perishablecount, {
				fastCount: true
			});
		} else {
			var adminperishablecount = tblPublish.find({
				$and: [{
					"OrderDetails.ShippingAddress.District": user.profile.Address.District
				}, {
					"Status": {
						$in: [Product_States[6], Product_States[10]]
					},
					"Perishable": "No"
				}]
			});
			Counts.publish(this, "publish.unassigned.nonperishable.unload.count", adminperishablecount, {
				fastCount: true
			});
		}
	}
});
Meteor.publish("publish.unassigned.perishable.unload.count", function () {
	var user = Meteor.users.findOne(this.userId);
	if (user) {
		if (user.profile.Role === "SuperAdmin") {
			var perishableTransport = tblTransportPerishable.find({
				"Status": Perishable_States[2],
				"CurrentAssign.Unload": ""
			});
			Counts.publish(this, "publish.unassigned.perishable.unload.count", perishableTransport, {
				fastCount: true
			});
		} else {
			var adminperishableTransport = tblTransportPerishable.find({
				"Status": Perishable_States[2],
				"CurrentAssign.Unload": "",
				"VehicleDetails.TravelTo.District": user.profile.Address.District
			});
			Counts.publish(this, "publish.unassigned.perishable.unload.count", adminperishableTransport, {
				fastCount: true
			});
		}
	}
});
Meteor.publish("publish.assigned.nonperishable.unload.count", function () {
	var user = Meteor.users.findOne(this.userId);
	if (user) {
		if (user.profile.Role === "SuperAdmin") {
			var perishablecount = tblPublish.find({
				"Status": {
					$in: [Product_States[11], Product_States[12]]
				},
				"Perishable": "No"
			});
			Counts.publish(this, "publish.assigned.nonperishable.unload.count", perishablecount, {
				fastCount: true
			});
		} else {
			var adminperishablecount = tblPublish.find({
				"Status": {
					$in: [Product_States[11], Product_States[12]]
				},
				"Perishable": "No",
				"OrderDetails.ShippingAddress.District": user.profile.Address.District
			});
			Counts.publish(this, "publish.assigned.nonperishable.unload.count", adminperishablecount, {
				fastCount: true
			});
		}
	}
});
Meteor.publish("publish.assigned.perishable.unload.count", function () {
	var user = Meteor.users.findOne(this.userId);
	if (user) {
		if (user.profile.Role === "SuperAdmin") {
			var perishableTransport = tblTransportPerishable.find({
				"Status": "UnloadAssigned",
				"CurrentAssign": {
					$ne: ""
				}
			});
			Counts.publish(this, "publish.assigned.perishable.unload.count", perishableTransport, {
				fastCount: true
			});
		} else {
			var adminperishableTransport = tblTransportPerishable.find({
				"Status": "UnloadAssigned",
				"CurrentAssign": {
					$ne: ""
				},
				"VehicleDetails.TravelTo.District": user.profile.Address.District
			});
			Counts.publish(this, "publish.assigned.perishable.unload.count", adminperishableTransport, {
				fastCount: true
			});
		}
	}
});
Meteor.publish("publish.all", function () {
	return tblPublish.find({});
})