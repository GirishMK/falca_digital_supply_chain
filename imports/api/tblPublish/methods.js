import { tblPublish } from './tblPublish.js';
import { tblNotification } from '../tblNotification/tblNotification.js';
import { Mongo } from 'meteor/mongo';
import { Product_States, Payment_Recieving_Flags } from '../extras/constants.js';
import { log } from "/imports/startup/both/index.js";
var shortid = require('shortid');
import { check, Match } from 'meteor/check';

//create productid here 
shortid.randomProductID = function (productname) {
    try {
        if (productname) {
            var productnameStartwith = productname.slice(0, 3);
            return (((productnameStartwith ? productnameStartwith : "") + shortid.generate().slice(0, 5)).toUpperCase());
        }
    } catch (e) {
        console.log("randomProductID at submission.js");
        console.log(e);
    }
}

Meteor.methods({
    'updateProductStatus': function (product, obj) {
        try {
            log.debug("Updating Product status Product Id" + product, {}, Meteor.userId());
            var getProduct = tblPublish.find({ "_id": new Mongo.ObjectID(product) }).fetch()[0];
            var transport = tblTransportPerishable.find({
                "LoadDetails.ProductId": product,
                "UnloadDetails.ProductId": product,
            }).fetch()[0];
            if (getProduct.Perishable == "No") {
                return tblPublish.update(
                    {
                        "_id": new Mongo.ObjectID(product)
                    },
                    {
                        $set: {
                            "FarmerInfo.AccountNumber": obj.AccountNumber,
                            "Status": "Shipped"
                        },
                        $push: {
                            "TransportDetails": transport._id._str,
                        }
                    }
                )
            } else {
                return tblPublish.update(
                    {
                        "_id": new Mongo.ObjectID(product),
                        "FinalizedBy.id": ""
                    },
                    {
                        $set: {
                            "ProductDetails.Weight.InitialUnit": obj.InitialUnit,
                            "ProductDetails.Price.InitialFarmerPrice": obj.InitialFarmerPrice,
                            "ProductDetails.Weight.Unit": obj.Unit,
                            "ProductDetails.Price.FarmerPrice": obj.FarmerPrice,
                            "FarmerInfo.AccountNumber": obj.AccountNumber,
                            "FinalizedBy.$.id": getProduct.ApprovedBy.id
                        },
                        $push: {
                            "TransportDetails": transport._id._str,
                        }
                    }
                )
            }


        }
        catch (error) {
            log.error("Error: Updating Product status failed Product Id" + product + " | Error: " + error, {}, Meteor.userId());
            throw error;
        }
    },
    "cancel.ProductPost": function (godownProdObj) {
        try {
            log.debug("updating in godownProducts.| ", {}, Meteor.userId());
            var updatedrow = tblPublish.update({
                "_id": new Mongo.ObjectID(godownProdObj.ProductId),
            },
                {
                    $set: { "Status": Product_States[9] }
                });
            log.debug("cancelling godownpostedproduct successfully.", {}, Meteor.userId());
            return updatedrow;
        } catch (error) {
            log.error("Error: Unable to cancell into godownpostedproduct Info |Error: " + error, {}, Meteor.userId());
        }
    },
    //creating commodity
    "createcommodity": function (productNameVar) {
        check(productNameVar, {
            FarmerId: String,
            FarmerInfo: {
                Name: String,
                Phone: String,
            },
            PostedBy: String,
            ApprovedBy: {
                id: String,
                Phone: String,
                Name: String,
                assignedBy: String,
                assignedDate: String,
            },
            FinalizedBy: [{
                id: String,
                Phone: String,
                Name: String,
                assignedBy: String,
                assignedDate: String,
            }],
            Watchlist: [String],
            ProductDetails: {
                Name: String,
                Category: String,
                Specifications: [{
                    Type: String,
                    Value: String
                }],
                SpecificationString: String,
                Weight: {
                    Qty: Number,
                    InitialQty: Number,
                    Unit: String,
                    QuantityInKg: Number,
                    NoofBags: Number,
                },
                img: {
                    uploaded: [String],
                    default: String,
                    variety: String
                },
                Location: {
                    Street: String,
                    District: String,
                    State: String,
                    Country: String,
                    Locality: String,
                    PostalCode: String
                },
                FullAddress: String,
                FormattedAddress: String,
                Coordinates: {
                    lat: Number,
                    lng: Number
                },
                Price: {
                    FarmerPrice: Number,
                },
                ModifiedDate: Date,
                FirstVisitDate: Match.OneOf(Date, null),
                FinalVisitDate: Match.OneOf(Date, null)
            },
            TotalBids: Match.Integer,
            TotalActiveBids: Match.Integer,
            MaxBid: Number,
            BidDetails: [{
                ConsumerId: String,
                Bids: [{
                    ConsumerPrice: Number,
                    Status: String,
                    Date: Date,
                    Remark: String,
                    FarmerPrice: Number,
                }],
            }],
            BidAcceptedDate: Match.OneOf(Date, null),
            OrderDetails: {
                OrderId: String,
                ShippingAddress: Match.ObjectIncluding({
                    District: String,
                    State: String
                }),
                ConsumerId: String,
                Price: {
                    PricePerUnitFarmer: Number,
                    PricePerUnitConsumer: Number,
                    TotalPrice: Number,
                    Tax: Number,
                },
                Weight: {
                    Qty: Match.Integer,
                    Unit: String
                },
                ShippedDate: Match.OneOf(Date, null),
                DeliveryDate: Match.OneOf(Date, null),
                Status: String,
            },
            ShipProductTo: String,
            Perishable: String,
            AvailableBy: String,
            Status: String
        });
        try {
            if (productNameVar != null) {
                var IsAvailable = tblPublish.insert({
                    "_id": new Mongo.ObjectID(),
                    "ProductId": shortid.randomProductID(productNameVar.ProductDetails.Name),
                    "FarmerId": productNameVar.FarmerId,
                    "FarmerInfo": productNameVar.FarmerInfo,
                    "PostedBy": productNameVar.PostedBy,
                    "ApprovedBy": productNameVar.ApprovedBy,
                    "FinalizedBy": productNameVar.FinalizedBy,
                    "Watchlist": productNameVar.Watchlist,
                    "ProductDetails": productNameVar.ProductDetails,
                    "TotalBids": productNameVar.TotalBids,
                    "TotalActiveBids": productNameVar.TotalActiveBids,
                    "MaxBid": 0,
                    "BidDetails": [],
                    "BidAcceptedDate": productNameVar.BidAcceptedDate,
                    "PaymentRecieved": 0,
                    "PaymentFromConsumer": false,
                    "OrderDetails": productNameVar.OrderDetails,
                    "ShipProductTo": productNameVar.ShipProductTo,
                    "AvailableBy": productNameVar.AvailableBy,
                    "TransportDetails": [],
                    "Perishable": productNameVar.Perishable,
                    "isValid": true,
                    "Status": productNameVar.Status,
                    "createdAt": new Date()
                });
            }
            // Send Notification only to particular district admin           
            // var admindistrict = Meteor.users.find({
            //     "profile.Role": "Admin",
            //     "profile.Address.District": Meteor.user().profile.Address.District
            // }).fetch();
            // var adminGCMIds = [];
            // for (var i = 0; i < admindistrict.length; i++) {
            //     if (admindistrict[i].profile.GCMID !== "") {
            //         adminGCMIds.push(admindistrict[i].profile.GCMID);
            //     }
            // }
            // var messageHeader = "Product Posted";
            // var messageString = productNameVar.ProductDetails.Name + " has been posted.\n Quantity: "
            //     + productNameVar.ProductDetails.Weight.Qty + " " + productNameVar.ProductDetails.Weight.Unit + "\n"
            //     + "Place:" + productNameVar.ProductDetails.FullAddress + "."
            // if (productNameVar.Perishable === "Yes") {
            //     var url = {
            //         page: "/unassignPerishableProduct",
            //         productId: IsAvailable._str,
            //     }
            // } else {
            //     var url = {
            //         page: "/unassignNonPerishableProduct",
            //         productId: IsAvailable._str,
            //     }
            // }
            // Meteor.call("sendPushNotification", adminGCMIds, messageHeader, messageString, url);
            log.info("Successfully inserted product. | ProductId: " + IsAvailable._str, {}, Meteor.userId());
            return IsAvailable;
        } catch (error) {
            console.log(error);
            log.error("Error occurred while inserting product | Error: " + error, {}, Meteor.userId());
            return error;
        }
    },

    // Function to assign a product which is in ApprovalPending stage to field team
    "assignProduct.toFieldTeam.stage1": function (productId, fieldTeamMemberId) {
        try {
            log.debug("Assigning product to FTM | To: Approved | ProductId: " + productId + " | FTMId: " + fieldTeamMemberId, {}, Meteor.userId());
            check(productId, String);
            check(fieldTeamMemberId, String);
            var firstVisitDate = new Date();
            firstVisitDate = firstVisitDate.setDate(firstVisitDate.getDate() + 1);
            firstVisitDate = new Date(firstVisitDate);
            var fieldTeamUser = (Meteor.users.find({ "_id": fieldTeamMemberId }).fetch())[0];
            if (fieldTeamUser) {
                var updateSuccess = tblPublish.update({
                    "_id": new Mongo.ObjectID(productId)
                }, {
                    $set: {
                        "ApprovedBy.id": fieldTeamMemberId,
                        "ApprovedBy.Name": fieldTeamUser.profile.Name,
                        "ApprovedBy.Phone": fieldTeamUser.profile.Phone.Primary,
                        "ApprovedBy.assignedBy": this.userId,
                        "ApprovedBy.assignedDate": new Date(),
                        "Status": "Approved",
                        "ProductDetails.FirstVisitDate": firstVisitDate,
                    }
                });

                if (updateSuccess) {
                    log.info("Success: Assigned product to FTM  | Stage: Product Updated | To: Approved | ProductId: " + productId + " | FTMId: " + fieldTeamMemberId, {}, Meteor.userId());
                    log.debug("Sending push notification");
                    var product = tblPublish.find({ "_id": new Mongo.ObjectID(productId) }).fetch()[0];
                    Meteor.call("fetch.gcmId.falcaperson", fieldTeamMemberId, function (error, gcmidarray) {
                        if (error) {
                            Meteor.call("Fetching gcmids of falca team member failed: " + error);
                            throw error;
                        } else {
                            if (gcmidarray) {
                                var farmerName = product.FarmerInfo.Name;
                                var location = product.ProductDetails.Location;
                                var weight = product.ProductDetails.Weight;
                                var productName = product.ProductDetails.Name;
                                var title = "New Product for Verification Assigned.";
                                var body = ("Farmer (" + farmerName + ")" +
                                    " from " + location.Street + " " + location.District + " " + location.State +
                                    " posted " + weight.Qty + " " + weight.Unit +
                                    " of " + productName);
                                var id = product._id._str;
                                var type = "Product_Stage1";
                                var url = {
                                    page: "/productApproval",
                                    productId: product._id._str,
                                    procuctName: product.ProductDetails.Name,
                                    productGenId: product.ProductId
                                }
                                Meteor.call("send.push.notification", gcmidarray, title, body, id, type, url);
                            }
                        }
                    })
                    log.info("Success: Assigned product to FTM  | Stage: Final | To: Approved | ProductId: " + productId + " | FTMId: " + fieldTeamMemberId, {}, Meteor.userId());
                    return updateSuccess;
                } else {
                    log.error("Error: Assigning product to FTM failed | Reason: " + updateSuccess + " | To: Approved | ProductId: " + productId + " | FTMId: " + fieldTeamMemberId, {}, Meteor.userId());
                    throw updateSuccess;
                }
            } else {
                log.error("Error: Assigning product to FTM failed | Reason: No FTM exists with given id | To: Approved | ProductId: " + productId + " | FTMId: " + fieldTeamMemberId, {}, Meteor.userId());
                throw "NO_FTM_WITH_USER_ID";
            }
        } catch (error) {
            log.error("Error: Assigning product to FTM failed | To: 1 | ProductId: " + productId + " | FTMId: " + fieldTeamMemberId + " | Error: " + error, {}, Meteor.userId());
            throw (error);
        }
    },
    // Function to assign a non perishable and perishable product for load
    "assignProduct.forload.stage2": function (productId, FinalizedBy, transportDetails) {
        try {
            log.debug("Assigning product to transport | To: InCheckout | ProductId: " + productId, {}, Meteor.userId());
            check(productId, String);
            check(FinalizedBy, [{
                id: String,
                Phone: String,
                Name: String,
                assignedBy: String,
                assignedDate: Date,
            }]);
            var updateSuccess = tblPublish.update({
                "_id": new Mongo.ObjectID(productId)
            }, {
                $set: {
                    "FinalizedBy": FinalizedBy,
                    "TransportDetails": transportDetails,
                    "Status": "InCheckout"
                }
            })
            if (updateSuccess) {
                log.info("Success: Assigned product to transport   | Stage: Product Updated | To: InCheckout | ProductId: " + productId, {}, Meteor.userId());
                // log.debug("Sending push notification");
                // var product = tblPublish.find({ "_id": new Mongo.ObjectID(productId) }).fetch()[0];
                // Meteor.call("fetch.gcmId.falcaperson", fieldTeamMemberId, function (error, gcmidarray) {
                //     if (error) {
                //         Meteor.call("Fetching gcmids of falca team member failed: " + error);
                //         throw error;
                //     } else {
                //         if (gcmidarray) {
                //             var farmerName = product.FarmerInfo.Name;
                //             var location = product.ProductDetails.Location;
                //             var weight = product.ProductDetails.Weight;
                //             var productName = product.ProductDetails.Name;
                //             var title = "New Product for Loading Assigned.";
                //             var body = ("Farmer (" + farmerName + ")" +
                //                 " from " + location.Street + " " + location.District + " " + location.State +
                //                 " has accepted bid on  " + weight.Qty + " " + weight.Unit +
                //                 " of " + productName);
                //             var id = product._id._str;
                //             var type = "Product_Stage2";
                //             var url = {
                //                 page: "/finalApproval",
                //                 productId: product._id._str,
                //                 procuctName: product.ProductDetails.Name,
                //                 productGenId: product.ProductId
                //             }

                //             Meteor.call("send.push.notification", gcmidarray, title, body, id, type, url);
                //         }
                //     }
                // })
                log.info("Success: Assigned transport to product  | Stage: Final | To: InCheckout | ProductId: " + productId);
                return updateSuccess;
            } else {
                log.error("Error: Assigning product transport to product failed | Reason: " + updateSuccess + " | To: InCheckout | ProductId: " + productId, {}, Meteor.userId());
                throw updateSuccess;
            }
        } catch (error) {
            log.error("Error: Assigning product to transport failed | To:: InCheckout | ProductId: " + productId + " | Error: " + error, {}, Meteor.userId());
            throw (error);
        }
    },
    "removeorAdd.transport.nonperishable.stage2": function (productId, transportDetails) {
        try {
            log.debug("Removing transport from product | ProductId: " + productId, {}, Meteor.userId());
            check(productId, String);
            if (productId) {
                var updateSuccess = tblPublish.update({
                    "_id": new Mongo.ObjectID(productId)
                }, {
                    $set: {
                        "TransportDetails": transportDetails
                    }
                });
                if (updateSuccess) {
                    return updateSuccess;
                } else {
                    log.error("Error: Cancelling product assignment from FTM failed | Reason: " + updateSuccess + " | To: BidAccepted | ProductId: " + productId, {}, Meteor.userId());
                    throw updateSuccess;
                }
            } else {
                log.error("Error: Removing transport from product failed | ProductId: " + productId, {}, Meteor.userId());
                throw "NO_FTM_WITH_USER_ID";
            }
        } catch (error) {
            log.error("Error: Removing transport from product failed | ProductId: " + productId + " | Error: " + error, {}, Meteor.userId());
            throw (error);
        }
    },

    // Function to cancel an assignment which is assigned to a field team member
    // Product in Approved stage, brought back the ApprovalPending stage
    "cancelAssignment.Product.stage1": function (productId, assignTo) {
        try {
            log.debug("Cancelling product assignment from FTM | To: ApprovalPending | ProductId: " + productId + " | FTMId: " + assignTo, {}, Meteor.userId());
            check(productId, String);
            check(assignTo, String);
            var fieldTeamUser = (Meteor.users.find({ "_id": assignTo }).fetch())[0];
            if (fieldTeamUser) {
                var updateSuccess = tblPublish.update({
                    "_id": new Mongo.ObjectID(productId)
                }, {
                    $set: {
                        "ApprovedBy.id": assignTo,
                        "ApprovedBy.Name": fieldTeamUser.profile.Name,
                        "ApprovedBy.Phone": fieldTeamUser.profile.Phone.primary,
                        "Status": "ApprovalPending"
                    }
                });
                if (updateSuccess) {
                    log.info("Success: Cancelled assignment from FTM | Stage: Product Updated | To: ApprovalPending | ProductId: " + productId + " | FTMId: " + assignTo, {}, Meteor.userId());
                    log.debug("Sending push notification");
                    var publish_product = tblPublish.find({
                        "_id": new Mongo.ObjectID(productId)
                    }).fetch()[0];
                    Meteor.call("fetch.gcmId.falcaperson", assignTo, function (error, gcmidarray) {
                        if (error) {
                            Meteor.call("Fetching gcmids of falca team member failed: " + error);
                            throw error;
                        } else {
                            if (gcmidarray) {
                                var location = (publish_product.ProductDetails.Location.District !== "") ? publish_product.ProductDetails.Location.District : publish_product.ProductDetails.Location.Locality;
                                var title = "Product Unassigned";
                                var body = "Product Name: " + publish_product.ProductDetails.Name + "(" + publish_product.ProductId + ")" +
                                    " District: " + location +
                                    " Quantity: " + publish_product.ProductDetails.Weight.Qty + publish_product.ProductDetails.Weight.Unit +
                                    " has been removed from your assignment list.";
                                var type = "Product_UnAssignment";
                                var id = publish_product._id._str;
                                Meteor.call("send.push.notification", gcmidarray, title, body, id, type);
                            }
                        }
                    })
                    log.info("Success: Cancelled assignment from FTM | Stage: Final | To: ApprovalPending | ProductId: " + productId + " | FTMId: " + assignTo, {}, Meteor.userId());
                    return updateSuccess;
                } else {
                    log.error("Error: Cancelling product assignment from FTM failed | Reason: " + updateSuccess + " | To: ApprovalPending | ProductId: " + productId + " | FTMId: " + assignTo, {}, Meteor.userId());
                    throw updateSuccess;
                }
            } else {
                log.error("Error: Cancelling product assignment from FTM failed | Reason: No FTM exists with given id | To: ApprovalPending | ProductId: " + productId + " | FTMId: " + assignTo, {}, Meteor.userId());
                throw "NO_FTM_WITH_USER_ID";
            }
        } catch (error) {
            log.error("Error: Cancelling product assignment from FTM failed | To:: ApprovalPending | ProductId: " + productId + " | FTMId: " + assignTo + " | Error: " + error, {}, Meteor.userId());
            throw (error);
        }
    },
    //cancel non perishable product from assigned load 
    "cancelAssignment.NonPerishable.stage2": function (productId) {
        try {
            log.debug("Cancelling product assignment from FTM | To: BidAccepted | ProductId: " + productId, {}, Meteor.userId());
            check(productId, String);
            if (productId) {
                var updateSuccess = tblPublish.update({
                    "_id": new Mongo.ObjectID(productId)
                }, {
                    $set: {
                        "FinalizedBy": [],
                        "Status": "BidAccepted"
                    }
                });
                if (updateSuccess) {
                    return updateSuccess;
                } else {
                    log.error("Error: Cancelling product assignment from FTM failed | Reason: " + updateSuccess + " | To: BidAccepted | ProductId: " + productId, {}, Meteor.userId());
                    throw updateSuccess;
                }
            } else {
                log.error("Error: Cancelling product assignment from FTM failed | Reason: No FTM exists with given id | To: BidAccepted | ProductId: " + productId, {}, Meteor.userId());
                throw "NO_FTM_WITH_USER_ID";
            }
        } catch (error) {
            log.error("Error: Cancelling product assignment from FTM failed | To:: BidAccepted | ProductId: " + productId + " | Error: " + error, {}, Meteor.userId());
            throw (error);
        }
    },
    "assignUnloadProductto_FieldTeam": function (productId, fieldTeamMemberId) {
        try {
            log.debug("Assigning Unloadproduct to FTM | To: Unload | ProductId: " + productId + " | FTMId: " + fieldTeamMemberId, {}, Meteor.userId());
            check(productId, String);
            check(fieldTeamMemberId, String);
            var fieldTeamUser = (Meteor.users.find({ "_id": fieldTeamMemberId }).fetch())[0];
            if (fieldTeamUser) {
                var unloadingProductStatus = tblPublish.find({ "_id": new Mongo.ObjectID(productId) }).fetch()[0];
                switch (unloadingProductStatus.Status) {
                    case "Shipped":
                        var updateSuccess = tblPublish.update({
                            "_id": new Mongo.ObjectID(productId)
                        }, {
                            $set: {
                                "UnloadStage": {
                                    "FieldMemberId": fieldTeamMemberId,
                                    "FieldMemberName": fieldTeamUser.profile.Name,
                                    "FieldMemberPhone": fieldTeamUser.profile.Phone.Primary,
                                    "AssignedBy": this.userId,
                                    "AssignedDate": new Date(),
                                },
                                "Status": Product_States[11],
                            }
                        });
                        if (updateSuccess) {
                            log.info("Success:Unloading Assign product to FTM  | Stage: Product Updated | To: Shipped | ProductId: " + productId + " | FTMId: " + fieldTeamMemberId, {}, Meteor.userId());
                            log.debug("Sending push notification");
                            var product = tblPublish.find({ "_id": new Mongo.ObjectID(productId) }).fetch()[0];
                            Meteor.call("fetch.gcmId.falcaperson", fieldTeamMemberId, function (error, gcmidarray) {
                                if (error) {
                                    Meteor.call("Fetching gcmids of falca team member failed: " + error);
                                    throw error;
                                } else {
                                    if (gcmidarray) {
                                        var farmerName = product.FarmerInfo.Name;
                                        var location = product.ProductDetails.Location;
                                        var weight = product.ProductDetails.Weight;
                                        var productName = product.ProductDetails.Name;
                                        var title = "New Product for Unloading Assigned.";
                                        var body = ("New product" + productName + "is assigned for unloading ");
                                        var id = product._id._str;
                                        var type = "UnloadingStage";
                                        var url = {
                                            page: "/unloadApproval",
                                            productId: product._id._str,
                                            procuctName: product.ProductDetails.Name,
                                            productGenId: product.ProductId
                                        }
                                        Meteor.call("send.push.notification", gcmidarray, title, body, id, type, url);
                                    }
                                }
                            })
                            log.info("Success: Unloading product Assigned to FTM  | Stage: Final | To: Shipped | ProductId: " + productId + " | FTMId: " + fieldTeamMemberId, {}, Meteor.userId());
                            return updateSuccess;
                        }
                        log.error("Error: Unloading product to FTM failed | Reason: " + updateSuccess + " | To: Shipped | ProductId: " + productId + " | FTMId: " + fieldTeamMemberId, {}, Meteor.userId());
                        break;

                    case "PartialShipped":
                        var partialupdateSuccess = tblPublish.update({
                            "_id": new Mongo.ObjectID(productId)
                        }, {
                            $set: {
                                "UnloadStage": {
                                    "FieldMemberId": fieldTeamMemberId,
                                    "FieldMemberName": fieldTeamUser.profile.Name,
                                    "FieldMemberPhone": fieldTeamUser.profile.Phone.Primary,
                                    "AssignedBy": this.userId,
                                    "AssignedDate": new Date(),
                                },
                                "Status": Product_States[12],
                            }
                        });
                        if (partialupdateSuccess) {
                            log.info("Success:Unloading Assign product to FTM  | Stage: Product Updated | To: Shipped | ProductId: " + productId + " | FTMId: " + fieldTeamMemberId, {}, Meteor.userId());
                            log.debug("Sending push notification");
                            var product = tblPublish.find({ "_id": new Mongo.ObjectID(productId) }).fetch()[0];
                            Meteor.call("fetch.gcmId.falcaperson", fieldTeamMemberId, function (error, gcmidarray) {
                                if (error) {
                                    Meteor.call("Fetching gcmids of falca team member failed: " + error);
                                    throw error;
                                } else {
                                    if (gcmidarray) {
                                        var farmerName = product.FarmerInfo.Name;
                                        var location = product.ProductDetails.Location;
                                        var weight = product.ProductDetails.Weight;
                                        var productName = product.ProductDetails.Name;
                                        var title = "New Product for Unloading Assigned.";
                                        var body = ("New product" + productName + "is assigned for unloading ");
                                        var id = product._id._str;
                                        var type = "UnloadingStage";
                                        var url = {
                                            page: "/unloadApproval",
                                            productId: product._id._str,
                                            procuctName: product.ProductDetails.Name,
                                            productGenId: product.ProductId
                                        }
                                        Meteor.call("send.push.notification", gcmidarray, title, body, id, type, url);
                                    }
                                }
                            })
                            log.info("Success: Unloading product Assigned to FTM  | Stage: Final | To: Shipped | ProductId: " + productId + " | FTMId: " + fieldTeamMemberId, {}, Meteor.userId());
                            return partialupdateSuccess;
                        }
                        log.error("Error: Unloading product to FTM failed | Reason: " + partialupdateSuccess + " | To: Shipped | ProductId: " + productId + " | FTMId: " + fieldTeamMemberId, {}, Meteor.userId());
                        break;
                }


            } else {
                log.error("Error: Unloading product to FTM failed | Reason: No FTM exists with given id | To: Shipped | ProductId: " + productId + " | FTMId: " + fieldTeamMemberId, {}, Meteor.userId());
                throw "NO_FTM_WITH_USER_ID";
            }
        } catch (error) {
            log.error("Error: Unloading product to FTM failed | To: 1 | ProductId: " + productId + " | FTMId: " + fieldTeamMemberId + " | Error: " + error, {}, Meteor.userId());
            throw (error);
        }
    },
    // Function to cancel an Unload Product assignment which is assigned to a field team member   
    "cancel_Unload_Assignment_Product": function (productId, assignTo) {
        try {
            log.debug("Cancelling Unload product assignment from FTM | To: ApprovalPending | ProductId: " + productId + " | FTMId: " + assignTo, {}, Meteor.userId());
            check(productId, String);
            check(assignTo, String);
            var fieldTeamUser = (Meteor.users.find({ "_id": assignTo }).fetch())[0];
            if (fieldTeamUser) {
                var cancleUnloadingProductStatus = tblPublish.find({ "_id": new Mongo.ObjectID(productId) }).fetch()[0];
                switch (cancleUnloadingProductStatus.Status) {
                    case "UnloadingAssigned":
                        var updateSuccess = tblPublish.update({
                            "_id": new Mongo.ObjectID(productId)
                        }, {
                            $set: {
                                "UnloadStage.FieldMemberId": "",
                                "UnloadStage.FieldMemberName": "",
                                "UnloadStage.FieldMemberPhone": "",
                                "Status": "Shipped"
                            }
                        });
                        if (updateSuccess) {
                            log.info("Success: Cancelled Unload assignment from FTM | Stage: Product Updated | To: ApprovalPending | ProductId: " + productId + " | FTMId: " + assignTo, {}, Meteor.userId());
                            log.debug("Sending push notification");
                            var publish_product = tblPublish.find({
                                "_id": new Mongo.ObjectID(productId)
                            }).fetch()[0];
                            Meteor.call("fetch.gcmId.falcaperson", assignTo, function (error, gcmidarray) {
                                if (error) {
                                    Meteor.call("Fetching gcmids of falca team member failed: " + error);
                                    throw error;
                                } else {
                                    if (gcmidarray) {
                                        var location = (publish_product.ProductDetails.Location.District !== "") ? publish_product.ProductDetails.Location.District : publish_product.ProductDetails.Location.Locality;
                                        var title = "Product Unassigned";
                                        var body = "Product Name: " + publish_product.ProductDetails.Name + "(" + publish_product.ProductId + ")" +
                                            " District: " + location +
                                            " Quantity: " + publish_product.ProductDetails.Weight.Qty + publish_product.ProductDetails.Weight.Unit +
                                            " has been removed from your assignment list.";
                                        var type = "Product_UnAssignment";
                                        var id = publish_product._id._str;
                                        Meteor.call("send.push.notification", gcmidarray, title, body, id, type);
                                    }
                                }
                            })
                            log.info("Success: Cancelled assignment from FTM | Stage: Final | To: ApprovalPending | ProductId: " + productId + " | FTMId: " + assignTo, {}, Meteor.userId());
                            return updateSuccess;
                        }
                        log.error("Error: Cancelling product assignment from FTM failed | Reason: " + updateSuccess + " | To: ApprovalPending | ProductId: " + productId + " | FTMId: " + assignTo, {}, Meteor.userId());
                        break;

                    case "PartialUnloadingAssigned":
                        var updateSuccess = tblPublish.update({
                            "_id": new Mongo.ObjectID(productId)
                        }, {
                            $set: {
                                "UnloadStage.FieldMemberId": "",
                                "UnloadStage.FieldMemberName": "",
                                "UnloadStage.FieldMemberPhone": "",
                                "Status": "PartialShipped"
                            }
                        });
                        if (updateSuccess) {
                            log.info("Success: Cancelled Unload assignment from FTM | Stage: Product Updated | To: ApprovalPending | ProductId: " + productId + " | FTMId: " + assignTo, {}, Meteor.userId());
                            log.debug("Sending push notification");
                            var publish_product = tblPublish.find({
                                "_id": new Mongo.ObjectID(productId)
                            }).fetch()[0];
                            Meteor.call("fetch.gcmId.falcaperson", assignTo, function (error, gcmidarray) {
                                if (error) {
                                    Meteor.call("Fetching gcmids of falca team member failed: " + error);
                                    throw error;
                                } else {
                                    if (gcmidarray) {
                                        var location = (publish_product.ProductDetails.Location.District !== "") ? publish_product.ProductDetails.Location.District : publish_product.ProductDetails.Location.Locality;
                                        var title = "Product Unassigned";
                                        var body = "Product Name: " + publish_product.ProductDetails.Name + "(" + publish_product.ProductId + ")" +
                                            " District: " + location +
                                            " Quantity: " + publish_product.ProductDetails.Weight.Qty + publish_product.ProductDetails.Weight.Unit +
                                            " has been removed from your assignment list.";
                                        var type = "Product_UnAssignment";
                                        var id = publish_product._id._str;
                                        Meteor.call("send.push.notification", gcmidarray, title, body, id, type);
                                    }
                                }
                            })
                            log.info("Success: Cancelled assignment from FTM | Stage: Final | To: ApprovalPending | ProductId: " + productId + " | FTMId: " + assignTo, {}, Meteor.userId());
                            return updateSuccess;
                        }
                        log.error("Error: Cancelling product assignment from FTM failed | Reason: " + updateSuccess + " | To: ApprovalPending | ProductId: " + productId + " | FTMId: " + assignTo, {}, Meteor.userId());
                        break;
                }

            } else {
                log.error("Error: Cancelling product assignment from FTM failed | Reason: No FTM exists with given id | To: ApprovalPending | ProductId: " + productId + " | FTMId: " + assignTo, {}, Meteor.userId());
                throw "NO_FTM_WITH_USER_ID";
            }
        } catch (error) {
            log.error("Error: Cancelling product assignment from FTM failed | To:: ApprovalPending | ProductId: " + productId + " | FTMId: " + assignTo + " | Error: " + error, {}, Meteor.userId());
            throw (error);
        }
    },
    "cancel_PerishableStage2": function (productId, status) {
        try {
            log.debug("Cancelling product assignment from FTM | ProductId: " + productId + "", Meteor.userId());
            check(productId, String);
            check(status, String);
            let updateSuccess = tblPublish.update({
                "_id": new Mongo.ObjectID(productId)
            }, {
                $set: {
                    "Status": status,
                    "FinalizedBy": [
                        {
                            "id": "",
                            "Phone": "",
                            "Name": "",
                            "assignedBy": "",
                            "assignedDate": ""
                        }
                    ]
                }
            });
            return updateSuccess;
        } catch (error) {
            log.error("Error: Cancelling product assignment from FTM failed | ProductId: " + productId + " | Error: " + error, {}, Meteor.userId());
            throw error;
        }
    },
    "UploadImage.publish": function (productId, fileObjId) {
        try {
            log.debug("Updating with product images | ProductId: " + productId + "", Meteor.userId());
            check(productId, String);
            check(fileObjId, String);
            let updateSuccess = tblPublish.update({
                "_id": new Mongo.ObjectID(productId)
            }, {
                $push: {
                    "ProductDetails.img.uploaded": fileObjId
                }
            });
            return updateSuccess;
        } catch (error) {
            log.error("Error: Updating images for product failed | ProductId: " + productId + " | Error: " + error, {}, Meteor.userId());
            throw error;
        }
    },
    "UploadAudio.publish": function (productId, fileObjId) {
        try {
            log.debug("Updating with product images | ProductId: " + productId + "", Meteor.userId());
            check(productId, String);
            check(fileObjId, String);
            let updateSuccess = tblPublish.update({
                "_id": new Mongo.ObjectID(productId)
            }, {
                $push: {
                    "ProductDetails.img.audio": fileObjId
                }
            });
            return updateSuccess;
        } catch (error) {
            log.error("Error: Updating images for product failed | ProductId: " + productId + " | Error: " + error, {}, Meteor.userId());
            throw error;
        }
    },
    "UploadVideo.publish": function (productId, fileObjId) {
        try {
            log.debug("Updating with product images | ProductId: " + productId + "", Meteor.userId());
            check(productId, String);
            check(fileObjId, String);
            let updateSuccess = tblPublish.update({
                "_id": new Mongo.ObjectID(productId)
            }, {
                $push: {
                    "ProductDetails.img.video": fileObjId
                }
            });
            return updateSuccess;
        } catch (error) {
            log.error("Error: Updating images for product failed | ProductId: " + productId + " | Error: " + error, {}, Meteor.userId());
            throw error;
        }
    },
    "insertAdminBid": function (object) {
        try {
            if (Meteor.userId()) {
                log.debug("Inserting a new bid. | ProductId: " + object.productId + " | BasePrice: " +
                    object.basePrice + "," + typeof basePrice, {}, Meteor.userId());
                var consumerId = Meteor.userId();
                if (object.EditBid == 0) {
                    tblPublish.update({
                        "ProductId": object.productId
                    },
                        {
                            $push: {
                                "BidDetails": {
                                    "ConsumerId": consumerId,
                                    "ShippingAddress": object.shippingAddress,
                                    "Status": object.Status !== undefined ? object.Status : "",
                                    "Bids": [{
                                        "ConsumerPrice": object.newPrice,
                                        "Status": "Waiting",
                                        "Date": new Date(),
                                        "Remark": "",
                                        "FarmerPrice": object.basePrice,
                                        "BidQuantity": object.bidQuantity !== "" ? object.bidQuantity : "",
                                        "Unit": object.Unit
                                    }]
                                }
                            },
                            $inc: {
                                "TotalBids": 1,
                                "TotalActiveBids": 1
                            },
                            $max: {
                                "MaxBid": Math.round((object.newPrice - object.farmerPrice) * Math.pow(10, 2)) / Math.pow(10, 2)
                            }
                        }
                    )
                }
                else {
                    tblPublish.update({
                        "ProductId": object.productId,
                        "BidDetails.ConsumerId": consumerId,
                    },
                        {
                            $set: {
                                "BidDetails.$.ShippingAddress": object.shippingAddress,
                                "BidDetails.$.Status": object.Status !== undefined ? object.Status : ""
                            },
                            $push: {
                                "BidDetails.$.Bids": {
                                    "ConsumerPrice": object.newPrice,
                                    "Status": "Waiting",
                                    "Date": new Date(),
                                    "Remark": "",
                                    "FarmerPrice": object.basePrice,
                                    "BidQuantity": object.bidQuantity !== "" ? object.bidQuantity : "",
                                    "Unit": object.Unit
                                }
                            },
                            $inc: {
                                "TotalBids": 1,
                                "TotalActiveBids": 1
                            },
                            $max: {
                                "MaxBid": Math.round((object.newPrice - object.farmerPrice) * Math.pow(10, 2)) / Math.pow(10, 2)
                            }
                        }
                    )
                }
            }
        } catch (error) {
            log.error("Error: Inserting bid failed. | ProductId: " + object.productId + " | Error: " + error, {}, Meteor.userId());
            throw error;
        }
    },

    //for falca to farmer
    "update.BidStatus": function (productId, price) {
        try {
            log.debug("Update bid status | ProductId: " + productId, {}, Meteor.userId());
            check(productId, String);
            var product = tblPublish.find({ "ProductId": productId }).fetch()[0];
            log.info("Success: Bid notification to Farmer | ProductId: " + productId + " | FarmerId: " + product.FarmerId, {}, Meteor.userId());
            log.debug("Sending push notification");
            Meteor.call("fetch.gcmId.farmer", product.FarmerId, function (error, gcmidarray) {
                if (error) {
                    Meteor.call("Fetching gcmids of Farmer failed: " + error);
                    throw error;
                } else {
                    if (gcmidarray) {
                        var title = "Your Product Bid";
                        var body = "Product" + " " + (product.ProductDetails.Name) + "(" + product.ProductId + ")" + " of quantity " + (product.ProductDetails.Weight.Qty + " " + product.ProductDetails.Weight.Unit) + " " + "has been bid by "
                            + Meteor.user().profile.Name + " from " + product.ProductDetails.Location.District + " for ₹ " + price + "/" + product.ProductDetails.Weight.Unit + ". ";
                        var id = product._id._str;
                        var type = "Product_Bade";
                        var url = {
                            page: "bid",
                            pid: product._id._str,
                            product: product.ProductDetails.Name
                        }
                        Meteor.call("send.push.notification", gcmidarray, title, body, id, type, url);
                    }
                }
            })
            log.info("Success: Bid sent to Farmer | ProductId: " + product.ProductId + " | FarmerId: " + product.FarmerId, {}, Meteor.userId());

        } catch (error) {
            log.error("Error: Bid Notification update failed | ProductId: " + productId + " | Error: " + error, {}, Meteor.userId());
            throw error;
        }
    }
})