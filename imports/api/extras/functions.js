import {
	textPattern,
	phonePattern,
	textNumberPattern,
	numberPattern,
	EmailCheck,
	pincodePattern
} from './constants.js'

import {
	tblPublish
} from "../tblPublish/tblPublish.js";
import {
	Perishable_States
} from './constants.js';
// function to validate form entries
export const validateFields = function (inputtype, message, Id) {
	var eorrorCount = [];
	switch (inputtype) {
		case "email":

			if ($(Id).val() === "") {
				highlightBox(Id);
				$(Id).parent().find("label").after("<div class='errormessage'> Enter " + message + "</div>");
				eorrorCount.push(message);
				break;
			}
			if (!EmailCheck.regex.test($(Id).val())) {
				highlightBox(Id);
				$(Id).parent().find("label").after("<div class='errormessage'> Enter valid email.</div>");
				eorrorCount.push(message);
				break;
			}
			break;

		case "select":

			var checkSelected = $(Id).val();
			if (checkSelected === "" || checkSelected === null) {
				highlightBox(Id);
				$(Id).parent().append("<div class='errormessage'> Select " + message + "</div>");
				eorrorCount.push(message);
				break;
			}
			break;

		case "text":

			if ($(Id).val() === "") {
				highlightBox(Id);
				$(Id).parent().find("label").after("<div class='errormessage'> Enter " + message + "</div>");
				eorrorCount.push(message);
				break;
			}
			if (!textPattern.texregex.test($(Id).val())) {
				highlightBox(Id);
				$(Id).parent().find("label").after("<div class='errormessage'> Allowed characters only.</div>");
				eorrorCount.push(message);
				break;
			}
			if (textPattern.maxlength < $(Id).val().length) {
				highlightBox(Id);
				$(Id).parent().find("label").after("<div class='errormessage'> limit exceeded </div>");
				eorrorCount.push(message);
				break;
			}
			break;

		case "batch":

			if ($(Id).val() === "") {
				highlightBox(Id);
				$(Id).parent().find("label").after("<div class='errormessage'> Enter " + message + "</div>");
				eorrorCount.push(message);
				break;
			}
			if (textPattern.maxlength < $(Id).val().length) {
				highlightBox(Id);
				$(Id).parent().find("label").after("<div class='errormessage'> limit exceeded </div>");
				eorrorCount.push(message);
				break;
			}
			break;

		case "phone":

			if ($(Id).val() === "") {
				highlightBox(Id);
				$(Id).parent().find("label").after("<div class='errormessage'> Enter " + message + "</div>");
				eorrorCount.push(message);
				break;
			}
			if (!phonePattern.phoneregex.test($(Id).val()) || phonePattern.length !== $(Id).val().length) {
				highlightBox(Id);
				$(Id).parent().find("label").after("<div class='errormessage'>Invalid Phone Number </div>");
				eorrorCount.push(message);
				break;
			}

			break;

		case "textnumber":

			if ($(Id).val() === "") {
				highlightBox(Id);
				$(Id).parent().find("label").after("<div class='errormessage'> Enter " + message + "</div>");
				eorrorCount.push(message);
				break;
			}
			if (!textNumberPattern.textNumberegex.test($(Id).val())) {
				highlightBox(Id);
				$(Id).parent().find("label").after("<div class='errormessage'> Invalid format</div>");
				eorrorCount.push(message);
				break;
			}
			if (textNumberPattern.maxlength < $(Id).val().length) {
				highlightBox(Id);
				$(Id).parent().find("label").after("<div class='errormessage'> limit exceeded </div>");
				eorrorCount.push(message);
				break;
			}
			break;

		case "number":

			if ($(Id).val() === "") {
				highlightBox(Id);
				$(Id).parent().find("label").after("<div class='errormessage'> Enter " + message + "</div>");
				eorrorCount.push(message);
				break;
			}
			if (!numberPattern.numberregex.test($(Id).val())) {
				highlightBox(Id);
				$(Id).parent().find("label").after("<div class='errormessage'>Zero is not allowed.</div>");
				eorrorCount.push(message);
				break;
			}
			if (numberPattern.maxlength < $(Id).val().length) {
				highlightBox(Id);
				$(Id).parent().find("label").after("<div class='errormessage'> limit exceeded</div>");
				eorrorCount.push(message);
				break;
			}
			break;


		case "pincode":

			if ($(Id).val() === "") {
				highlightBox(Id);
				$(Id).parent().find("label").after("<div class='errormessage'> Enter " + message + "</div>");
				eorrorCount.push(message);
				break;
			}
			if (!pincodePattern.pincoderegex.test($(Id).val()) || pincodePattern.length !== $(Id).val().length) {
				highlightBox(Id);
				$(Id).parent().find("label").after("<div class='errormessage'>Invalid pin code </div>");
				eorrorCount.push(message);
				break;
			}
			break;

		case "password":
			if ($(Id).val() === "") {
				highlightBox(Id);
				$(Id).parent().find("label").after("<div class='errormessage'> Enter " + message + "</div>");
				eorrorCount.push(message);
				break;
			}
			break;

		case "date":
			if ($(Id).val() === "") {
				highlightBox(Id);
				$(Id).parent().find("label").after("<div class='errormessage'> Enter " + message + "</div>");
				eorrorCount.push(message);
				break;
			}
			break;

		default:
			break;
	}
	if (eorrorCount.length > 0) {
		throw eorrorCount;
	}
}

// Function: highlightBox
// Parameters: id: html element to be highlighted
// Return: Nothing
var highlightBox = function (textboxId) {
	$(textboxId).focus();
	$(textboxId).addClass("invalid");
	$(textboxId).parent().find("label").addClass("active");
}


// function to generate a specific message using type and id
export const generateNotification = function (type, id, publish_product) {
	check(type, String);
	check(id, String);
	var messageObj = {};
	switch (type) {
		//mesaage for aadhar rejection
		case "Aadhaar_Rejection":
			var farmerId = id;
			var farmerinfo = Meteor.users.find({
				"_id": farmerId
			}).fetch()[0];
			var aadharImageUploaded = farmerinfo.profile.UID.aadharImageUploaded;
			var Value = farmerinfo.profile.UID.Value;
			if (aadharImageUploaded === true || Value === "MANUAL") {
				messageObj = {
					"Title": "Address-Proof Rejected",
					"Body": "Your address-proof has been rejected by Admin member " + publish_product.profile.Name + "(" + publish_product.profile.Phone.Primary + "). Please Re-link",
					"Image": "/img/defaultuser.png",
					"Type": type,
					"Id": farmerId
				};
			} else {
				messageObj = {
					"Title": "Address Proof Rejected",
					"Body": "Your address proof has been rejected by Admin member " + publish_product.profile.Name + "(" + publish_product.profile.Phone.Primary + "). Please Re-link",
					"Image": "/img/defaultuser.png",
					"Type": type,
					"Id": farmerId
				};
			}
			break;

		// message object for First stage assigned product
		case "Product_Assigned":
			var productId = id;
			var location = (publish_product.ProductDetails.Location.District !== "") ? publish_product.ProductDetails.Location.District : publish_product.ProductDetails.Location.Locality;
			messageObj = {
				"Title": "New Assigned Product",
				"Body": "A new product has been assigned to you." +
					"\nName: " + publish_product.ProductDetails.Name + "(" + publish_product.ProductId + ")" +
					"\nDistrict: " + location +
					"\nQuantity:" + publish_product.ProductDetails.Weight.Qty + publish_product.ProductDetails.Weight.Unit + ".",
				"Image": publish_product.ProductDetails.img.default,
				"Type": type,
				"Id": productId
			};
			break;

		// message for stage 2 assigned products
		case "Loading_Shipping_Assigned":
			var productId = id;
			var location = (publish_product.ProductDetails.Location.District !== "") ? publish_product.ProductDetails.Location.District : publish_product.ProductDetails.Location.Locality;
			messageObj = {
				"Title": "New Product for Loading",
				"Body": "A new product has been assigned to you for loading." +
					"\nName: " + publish_product.ProductDetails.Name + "(" + publish_product.ProductId + ")" +
					"\nDistrict: " + location +
					"\nQty: " + publish_product.ProductDetails.Weight.Qty + " " + publish_product.ProductDetails.Weight.Unit + ".",
				"Image": publish_product.ProductDetails.img.default,
				"Type": type,
				"Id": productId
			};
			break;

		//message for stage3 assigned product
		case "Unloading_Product_Assigned":
			var productId = id;
			var location = (publish_product.OrderDetails.ShippingAddress.District !== "") ? publish_product.OrderDetails.ShippingAddress.District : publish_product.OrderDetails.ShippingAddress.Address;
			messageObj = {
				"Title": "New Product for Unloading",
				"Body": "A new product has been assigned to you for Unloading." +
					"\nName: " + publish_product.ProductDetails.Name + "(" + publish_product.ProductId + ")" +
					"\nDistrict: " + location +
					"\nQuantity:" + publish_product.OrderDetails.Weight.Qty + publish_product.OrderDetails.Weight.Unit + ".",
				"Image": publish_product.ProductDetails.img.default,
				"Type": type,
				"Id": productId
			};
			break;
		case "Unloading_UnAssigned":
			var productId = id;
			var location = (publish_product.OrderDetails.ShippingAddress.District !== "") ? publish_product.OrderDetails.ShippingAddress.District : publish_product.OrderDetails.ShippingAddress.Address;
			messageObj = {
				"Title": "Unload Product Unassigned",
				"Body": "Product Name" +
					"\nName: " + publish_product.ProductDetails.Name + "(" + publish_product.ProductId + ")" +
					"\nDistrict: " + location +
					"\nQuantity:" + publish_product.OrderDetails.Weight.Qty + publish_product.OrderDetails.Weight.Unit + " has been removed from your assignment list.",
				"Image": publish_product.ProductDetails.img.default,
				"Type": type,
				"Id": productId
			};
			break;
		// This is to send notification for farmer when admin add farmer details when farmer registered only with
		// phone number
		case "Aadhaar_Accepted_ByAdmin":
			var farmerId = id;
			messageObj = {
				"Title": "Address Proof Verified",
				"Body": "Your address has been verified by Admin member " + publish_product.profile.Name + "(" + publish_product.profile.Phone.Primary + ").",
				"Image": "/img/defaultuser.png",
				"Type": type,
				"Id": farmerId
			};
			break;
		// message for new aadhar assigned
		case "Aadhar_Assigned":
			var farmerId = id;
			var value = publish_product.profile.UID.aadharImageUploaded;
			if (value === true) {
				messageObj = {
					"Title": "New Farmer Registered",
					"Body": "A new address-proof has been assigned to you for verification.",
					"Image": '/img/aadhar.png',
					"Type": type,
					"Id": farmerId
				};
			} else if (publish_product.profile.UID.Value === "MANUAL") {
				messageObj = {
					"Title": "New Farmer Registered",
					"Body": "A new address-proof has been assigned to you for verification." +
						"\nName: " + publish_product.profile.Name +
						"\nDistrict: " + publish_product.profile.Address.District + ".",
					"Image": '/img/aadhar.png',
					"Type": type,
					"Id": farmerId
				};
			} else {
				messageObj = {
					"Title": "New Farmer Registered",
					"Body": "A new address proof has been assigned to you for verification." +
						"\nName: " + publish_product.profile.Name +
						"\nDistrict: " + publish_product.profile.Address.District + ".",
					"Image": '/img/aadhar.png',
					"Type": type,
					"Id": farmerId
				};
			}
			break;
		case "Aadhar_UnAssigned":
			var farmerId = id;
			var value = publish_product.profile.UID.aadharImageUploaded;
			if (value === true || publish_product.profile.UID.Value === "MANUAL") {
				messageObj = {
					"Title": "Address-Proof Unassigned",
					"Body": "Address-proof has been removed from your assignment list",
					"Image": '/img/aadhar.png',
					"Type": type,
					"Id": farmerId
				};
			} else {
				messageObj = {
					"Title": "Address Proof Unassigned",
					"Body": "Address proof has been removed from your assignment list",
					"Image": '/img/aadhar.png',
					"Type": type,
					"Id": farmerId
				};
			}
			break;
		case "Product_UnAssigned":
			var productId = id;
			var location = (publish_product.ProductDetails.Location.District !== "") ? publish_product.ProductDetails.Location.District : publish_product.ProductDetails.Location.Locality;
			messageObj = {
				"Title": "Product Unassigned",
				"Body": "Product Name: " + publish_product.ProductDetails.Name + "(" + publish_product.ProductId + ")" +
					" District: " + location +
					" Quantity: " + publish_product.ProductDetails.Weight.Qty + publish_product.ProductDetails.Weight.Unit +
					" has been removed from your assignment list.",
				"Image": publish_product.ProductDetails.img.default,
				"Type": type,
				"Id": productId
			};
			break;
		case "Shipping_UnAssigned":
			var productId = id;
			var location = (publish_product.ProductDetails.Location.District !== "") ? publish_product.ProductDetails.Location.District : publish_product.ProductDetails.Location.Locality;
			messageObj = {
				"Title": "Shipping Unassigned",
				"Body": "Product Name: " + publish_product.ProductDetails.Name + "(" + publish_product.ProductId + ")" +
					" District: " + location +
					" Quantity: " + publish_product.ProductDetails.Weight.Qty + publish_product.ProductDetails.Weight.Unit +
					" for loading has been removed from your assignment list.",
				"Image": publish_product.ProductDetails.img.default,
				"Type": type,
				"Id": productId
			};
			break;
		case "NonPerishable_Loading_Cancel":
			var transportId = id;
			var transport = tblTransportPerishable.find({
				"_id": new Mongo.ObjectID(id),
				"Status": Perishable_States[1]
			}).fetch()[0];
			messageObj = {
				"Title": "Transport Unassigned",
				"Body": "An assigned transport with Batch Id :" + " " + transport.BatchId + " " + "for product name" +
					" " + transport.ProductName + " " + "has been removed from your assignment list.",
				"Type": type,
				"TransportId": transportId
			};
			break;

		// message for non perishable transport assigning for loading
		case "NonPerishable_Loading_Assign":
			var transportId = id;
			var transport = tblTransportPerishable.find({
				"_id": new Mongo.ObjectID(id),
				"Status": Perishable_States[1]
			}).fetch()[0];
			messageObj = {
				"Title": "New Transport for Loading",
				"Body": "A transport with Batch Id :" + transport.BatchId + " " + " starting from" + " " + transport.VehicleDetails.TravelFrom.District + " " + "for product" + " " + transport.ProductName + " " + "has been assigned for Loading",
				"Type": type,
				"TransportId": transportId
			};
			break;

		case "Consumer_Gst_Verification":
			var consumerId = id;
			messageObj = {
				"Title": "Verification Completed",
				"Body": "Your GST(" + publish_product.profile.UID.Value + ") has been verified and you can now bid the products.",
				"Image": "",
				"Type": type,
				"Id": consumerId
			};
			break;
		case "Consumer_Gst_Rejection":
			var consumerId = id;
			messageObj = {
				"Title": "Verification Rejected",
				"Body": "Your GST(" + publish_product.profile.UID.Value + ") has been rejected.",
				"Image": "",
				"Type": type,
				"Id": consumerId
			};
			break;
		// message for perishable transport assigning for loading
		case "Perishable_Loading_Assign":
			var transportId = id;
			messageObj = {
				"Title": "New Transport for Loading",
				"Body": "A transport with Batch Id :" + publish_product.BatchId + " " + " starting from" + " " + publish_product.VehicleDetails.TravelFrom.District + " " + "for product" + " " + publish_product.ProductName + " " + "has been assigned for Loading",
				"Type": type,
				"TransportId": transportId
			};
			break;
		// message for perishable transport loading cancel
		case "Perishable_Loading_Cancel":
			var transportId = id;
			messageObj = {
				"Title": "Transport Unassigned",
				"Body": "An assigned transport with Batch Id :" + " " + publish_product.BatchId + " " + "for product name" +
					" " + publish_product.ProductName + " " + "has been removed from your assignment list.",
				"Type": type,
				"TransportId": transportId
			};
			break;

		// message for perishable transport assigned for Unloading
		case "Unloading_Perishable_Product_Assigned":
			var transportId = id;
			if (publish_product.VehicleDetails.TotalBags.Load > 0) {
				messageObj = {
					"Title": "New case for Unloading",
					"Body": "A transport with Batch Id :" + publish_product.BatchId + " " + "starting from" + " " + publish_product.VehicleDetails.TravelFrom.District + " " + "for product" + " " + publish_product.ProductName +
						" comprising of" + " " + publish_product.VehicleDetails.TotalBags.Load + "bags" + " " +
						"has been assigned for Unloading.",
					"Type": type,
					"TransportId": transportId
				};
			} else {
				messageObj = {
					"Title": "New case for Unloading",
					"Body": "A transport with Batch Id :" + publish_product.BatchId + " " + "starting from" + " " + publish_product.VehicleDetails.TravelFrom.District + " " + "for product" + " " + publish_product.ProductName +
						" has been assigned for Unloading.",
					"Type": type,
					"TransportId": transportId
				};
			}
			break;
		// message for perishable transport loading cancel
		case "Unloading_Perishable_Product_Cancel":
			var transportId = id;
			messageObj = {
				"Title": "Transport Unassigned",
				"Body": "An assigned transport with Batch Id :" + " " + publish_product.BatchId + " " + "for product name" +
					" " + publish_product.ProductName + " " + "has been removed from your assignment list.",
				"Type": type,
				"TransportId": transportId
			};
			break;

		// message for preorder assign
		case "PreOrder_Assign":
			var orderId = id;
			messageObj = {
				"Title": "Preorder Assigned",
				"Body": "Preorder for product " + publish_product.ProductName +
					" of Quantity " + publish_product.TotalQty + "Kg has been assigned.",
				"Type": type,
				"Id": orderId
			};
			break;

		// message for preorder cancel
		case "PreOrder_Cancel":
			var orderId = id;
			messageObj = {
				"Title": "Preorder Cancelled",
				"Body": "Preorder for product " + publish_product.ProductName +
					" with " + publish_product.Grade + " of Quantity " + publish_product.Qty + "Kg has been cancelled. Due to " + publish_product.CancelReason,
				"Type": type,
				"Id": orderId
			};
			break;
		//  message object for product Posted.(notification to Consumer)
		case "Product_Posted":
			var productId = id;
			messageObj = {
				"Title": "Preferenced Product",
				"Body": publish_product.ProductDetails.Name + " has been posted.\n Quantity: " +
					publish_product.ProductDetails.Weight.Qty + " " + publish_product.ProductDetails.Weight.Unit + "\n" +
					"Place:" + publish_product.ProductDetails.FullAddress + ".",
				"Image": publish_product.ProductDetails.img.default,
				"Type": type,
				"Id": productId
			};
			break;
		case "bid_to_farmer":
			var productId = id;
			var product = tblPublish.find({
				"ProductId": id
			}).fetch()[0];
			messageObj = {
				"Title": "New Bid",
				"Body": "Product" + " " + (product.ProductDetails.Name) + "(" + product.ProductId + ")" + " of quantity " + (product.ProductDetails.Weight.Qty + " " + product.ProductDetails.Weight.Unit) + " " + "has been bid by " +
					" Falca Consumer from " + product.ProductDetails.Location.District + ". ",
				"Image": product.ProductDetails.img.default,
				"Type": "Product_Bade",
				"Id": product._id._str
			};
			break;
		default:
			break;
	}
	return messageObj;
}

export const generatePushNotification = function (farmerId, backendId, title, body, type) {
	check(farmerId, String);
	check(backendId, String);
	var farmer = (Meteor.users.find({
		"_id": farmerId
	}).fetch())[0];
	if (farmer) {
		Meteor.call("fetch.gcmId.falcaperson", backendId, function (error, gcmidarray) {
			if (error) {
				Meteor.call("Fetching gcmids of falca team member failed: " + error);
				throw error;
			} else {
				if (gcmidarray) {
					Meteor.call("send.push.notification", gcmidarray, title, body, farmerId, type);
				}
			}
		})
	}
}