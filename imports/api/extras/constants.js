export const Categories = ["Vegetables", "Fruits", "Pulses", "Spices", "Cereals", "Flowers", "OilSeeds"];
export const Units = ["KG", "Quintal", "50KG Bag", "Tonne"];

export const BackendTeamApkLink = Meteor.settings.public.ApkLink + "/AppUpdate/FieldApp/FieldApp.apk";
export const AdminTeamApkLink = Meteor.settings.public.ApkLink + "/AppUpdate/AdminApp/AdminApp.apk";

export const EmailCheck = {
    regex: /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i,
    maxlength: 50
}
export const textPattern = {
    texregex: /^[a-zA-Z ]+$/,
    maxlength: 50
}
export const phonePattern = {
    phoneregex: /[6-9][0-9]{9}/,
    length: 10
}
export const textNumberPattern = {
    textNumberegex: /^[a-zA-Z0-9-/#, ]*$/,
    maxlength: 50,
}
export const numberPattern = {
    numberregex: /^[1-9][0-9]*$/,
    maxlength: 8,
}
export const pincodePattern = {
    pincoderegex: /^[1-9][0-9]*$/,
    length: 6
}


/*For GCM AUTH */
export const GCMURL = Meteor.settings.public.GCMURL;
export const AuthKey = Meteor.settings.public.pushNoti_AuthKey;
/*End of GCM AUTH */

/*For Unassigned aadhar */
export const MAXIMUM_PRODUCTS_LIMIT = 5;
export const MAXIMUM_PRODUCTS_LIMIT_NOTIF = 10;
export const Minimum_Images_To_Upload = 2;

/* Product Stages */
export const Product_States = {
    1: "ApprovalPending",
    2: "Approved",
    3: "Available",
    4: "BidAccepted",
    5: "InCheckout",
    6: "Shipped",
    7: "Delievered",
    8: "Rejected",
    9: "Deleted",
    10: "PartialShipped",
    11: "UnloadingAssigned",
    12: "PartialUnloadingAssigned",
    13: "AvailableToAdmin"
}

export const Preorders_States = {
    1: "Active",
    2: "Confirmed",
    3: "Shipped",
    4: "Delivered",
    5: "Cancelled"
}
export const localTransportStatus = {
    1: "Active",
    2: "Started",
    3: "Delivered",
    4: "Cancelled"
}

/* Payment Recieving flags */
export const Payment_Recieving_Flags = {
    "Payment_From_Consumer": "FRC",
    "Payment_To_Farmer": "TOF"
}

export const Perishable_States = {
    1: "Active",
    2: "Loaded",
    3: "UnloadAssigned",
    4: "Unloaded",
    5: "SalesAssigned",
    6: "SalesCompleted",
    7: "AddedToInventory"
}