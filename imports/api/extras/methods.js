import { BackendTeamApkLink,AdminTeamApkLink} from './constants.js';
import { GCMURL, AuthKey } from './constants.js';
import { log } from  "/imports/startup/both/index.js";

Meteor.methods({
	// General function to send emails with a subject and body of the email
	"send.email": function (to, subject, text) {
		var from = Meteor.settings.public.send_email_from;
		process.env.MAIL_URL = 'smtp://support%40falcasolutions.com:' + encodeURIComponent("Welcome@@1") + '@smtp.gmail.com:587';
		// Make sure that all arguments are strings.
		check([to, from, subject, text], [String]);
		// Let other method calls from the same client start running, without
		// waiting for the email sending to complete.
		this.unblock();
		log.debug("Sending Email | Stage: Final | To: " + to + " | Subject: " + subject, {}, Meteor.userId());
		try {
			Email.send({
				to: to,
				from: from,
				subject: subject,
				text: text
			});
			log.info("Success: Email Sent | To: " + to + " | Subject: " + subject, {}, Meteor.userId());
			return true;
		} catch (error) {
			log.error("Error: Sending email failed | To: " + to + " | Subject: " + subject + " | Error: " + error, {}, Meteor.userId());
			throw error;
		}
	},

	// Function to send registration mails to field person.
	"send.registration.email": function (to, password) {
		check(to, String);
		check(password, String);
		var subject = "FALCA: Invitation to join FALCA team member.";
		var text = "You are invited for the post of a FALCA field team member.\n" +
			"Please download the apk from the following link: " + BackendTeamApkLink +
			"\n Please login using following credentials:" +
			"\n email: " + to +
			"\n password: " + password +
			"\n Note: This is only One time access password. If in case of losing your phone or reinstalling the app please contact admin for a new password." +
			"\n This is a computer generated email, please do not reply.";
		log.debug("Sending Email | Stage: Creating Registration Email | To: " + to + " | Subject: " + subject, {}, Meteor.userId());
		return Meteor.call("send.email", to, subject, text);
	},
	"send.salesregistration.email": function (to, password) {
		check(to, String);
		check(password, String);
		var subject = "FALCA: Invitation to join FALCA sales person.";
		var text = "You are invited for the post of a FALCA sales person.\n" +
			"Please download the apk from the following link: " + BackendTeamApkLink +
			"\n Please login using following credentials:" +
			"\n email: " + to +
			"\n password: " + password +
			"\n Note: This is only One time access password. If in case of losing your phone or reinstalling the app please contact admin for a new password." +
			"\n This is a computer generated email, please do not reply.";
		log.debug("Sending Email | Stage: Creating Registration Email | To: " + to + " | Subject: " + subject, {}, Meteor.userId());
		return Meteor.call("send.email", to, subject, text);
	},
	"send.storeregistration.email": function (to, password) {
		check(to, String);
		check(password, String);
		var subject = "FALCA: Invitation to join FALCA store Manager.";
		var text = "You are invited for the post of a FALCA store Manager.\n" +
			"Please download the apk from the following link: " + AdminTeamApkLink +
			"\n Please login using following credentials:" +
			"\n email: " + to +
			"\n password: " + password +
			"\n Note: This is only One time access password. If in case of losing your phone or reinstalling the app please contact admin for a new password." +
			"\n This is a computer generated email, please do not reply.";
		log.debug("Sending Email | Stage: Creating Registration Email | To: " + to + " | Subject: " + subject, {}, Meteor.userId());
		return Meteor.call("send.email", to, subject, text);
	},

	// Function to send registration mails to admin person.
	"send.adminregistration.email": function (to, password) {
		check(to, String);
		check(password, String);
		var subject = "FALCA: Invitation to join FALCA Admin Member.";
		var text = "You are invited for the post of a FALCA Admin member.\n" +			
			"\n Please login using following credentials:" +
			"\n email: " + to +
			"\n password: " + password +
			"\n Note: This is only One time access password." +
			"\n You can also download the apk from the following link: " + AdminTeamApkLink +
			"\n This is a computer generated email, please do not reply.";
		log.debug("Sending Email | Stage: Creating Registration Email | To: " + to + " | Subject: " + subject, {}, Meteor.userId());
		return Meteor.call("send.email", to, subject, text);
	},

	// Function to send push notifications
	"send.push.notification": function (deviceIds, title, message, id, type, url) {
		try {
			if (typeof deviceIds !== "undefined") {
			log.debug("Sending push notification | DeviceIds: " + deviceIds.toString(), {}, Meteor.userId());
			HTTP.call('POST', GCMURL, {
				data:
					{
						"delay_while_idle": true,
						"data": {
							"image": Meteor.settings.public.GCM_image+"/assets/images/logo.png",
							"title": title,
							"body": message,
							"count": 1,
							"id": id,
							"type": type,
							"forceShow": true,
							"icon": "",
							"url": JSON.stringify(url)
						},
						"registration_ids": deviceIds
					},
				headers: {
					"Content-Type": "application/json",
					"Authorization": "key=" + AuthKey,
				}
			}, function (error) {
				if (error) {
					log.error("Error: Push notification sending failed | DeviceIds: " + deviceIds.toString() + " | Error: " + error, {}, Meteor.userId());
				} else {
					log.info("Success: Push notification sent | DeviceIds: " + deviceIds.toString(), {}, Meteor.userId());
				}
			});
		}
		} catch (error) {
			log.error("Error: Push notification sending failed | DeviceIds: " + deviceIds.toString() + " | Error: " + error, {}, Meteor.userId());
		}
	}
})