
import { Meteor } from 'meteor/meteor';
import { tblPerishableGrade } from '../tblPerishableGrade.js';


Meteor.publish('perishableGrade.all', function () {
	return tblPerishableGrade.find();
});