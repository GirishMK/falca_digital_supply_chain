// Import modules used by both client and server through a single index entry point
// e.g. useraccounts configuration file.

// Logging configuration
import { Logger } from 'meteor/ostrio:logger';
import { LoggerFile } from 'meteor/ostrio:loggerfile';

const logfile_settings = {
	fileNameFormat(time) {
		// Create log-files hourly
		return (time.getDate()) + "-" + (time.getMonth() + 1) + "-" + (time.getFullYear()) + "_" + (time.getHours()) + ".log";
	},
	format(time, level, message, data, userId) {
		// Omit Date and hours from messages
		return "[" + level + "] | " + (time.getHours()) + ":" + (time.getMinutes()) + ":" + (time.getSeconds()) + " | \"" + message + "\" | User: " + userId + "\r\n";
	},
	path: '/data/Adminlogs' // Use absolute storage path
}

const logger_config = {
	enable: true,
	filter: ['ERROR', 'FATAL', 'WARN', 'INFO'], // Filters: 'ERROR', 'FATAL', 'WARN', 'DEBUG', 'INFO', 'TRACE', '*'
	client: true, // Set to `false` to avoid Client to Server logs transfer
	server: true  // Allow logging on server
}
export const log = new Logger();
(new LoggerFile(log, logfile_settings)).enable(logger_config);