// Import client startup through a single index entry point

// Configuring user accounts
import './routes.js';
import { Accounts } from 'meteor/accounts-base';
// Accounts.ui.config({
//     passwordSignupFields: 'USERNAME_ONLY'
// });
Meteor.startup(function () {
    BackBehaviour.attachToHardwareBackButton(true);   
    // You can later unattach and revert to the default behaviour with
    // BackBehaviour.attachToHardwareBackButton(false);
});
// Catching all client errors
import { log } from "../both/index.js";
/* Store original window.onerror */
const _GlobalErrorHandler = window.onerror;

window.onerror = (msg, url, line) => {
    log.error(msg, { file: url, onLine: line });
    if (_GlobalErrorHandler) {
        _GlobalErrorHandler.apply(this, arguments);
    }
};

