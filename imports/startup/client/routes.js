import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { log } from '../both/index.js';

// Import needed templates

//layout start
import '../../ui/layouts/commonheaderLayout/commonheaderLayout.js';
import '../../ui/layouts/body/body.js';
import '../../ui/layouts/footer/footer.js';
import '../../ui/layouts/loginlayout/loginlayout.js';
//layout end

import '../../ui/components/card_assignment/card_assignment.js';
import '../../ui/components/nodata/nodata.js';
import '../../ui/components/commonheader/commonheader.js';
import '../../ui/components/login/login.js';
import '../../ui/components/noInternet/noInternet.js';

//heade side menu files start
import '../../ui/components/home/home.js';
import '../../ui/components/profile/profile.js';
import '../../ui/components/notification/notification.js';

//header side menu files end

// default Route
FlowRouter.route('/', {
	action: function () {
		Tracker.autorun(function () {
			$(window).scrollTop(0);
			if (Meteor.userId()) {
				FlowRouter.go('/home');
			} else {
				FlowRouter.go('/login');
			}
		});
	},
	onBack: function () { }
});
// Route to home login page 
FlowRouter.route('/login', {
	action: function () {
		Tracker.autorun(function () {
			$(window).scrollTop(0);
			if (Meteor.userId()) {
				FlowRouter.go('/');
			} else {
				BlazeLayout.render("loginlayout", {
					content: "login"
				});
			}
		});
	},
	onBack: function () { }
});
// Route to home
FlowRouter.route('/home', {
	name: 'App.home',
	action() {
		$(window).scrollTop(0);
		log.info("Home Page Loaded");
		BlazeLayout.render('App_body', {
			main: 'homepage'
		});
	},
});
//aadhaar card route start
FlowRouter.route('/registeredFarmer', {
	name: "Common header",
	action() {
		$(window).scrollTop(0);
		log.info("Common Header Loaded");
		BlazeLayout.render('commonheaderLayout', {
			main: 'registeredFarmer',
			title: "Address"
		});
	},
	onBack: function () {
		$(window).scrollTop(0);
		FlowRouter.go('/home');
	}
});

//Route to Profile
FlowRouter.route('/profile', {
	name: "profile",
	action: function () {
		$(window).scrollTop(0);
		log.info("Profile Page Loaded");
		BlazeLayout.render('commonheaderLayout', {
			main: "profile",
			title: "Profile"
		});
	},
	onBack: function () {
		$(window).scrollTop(0);
		FlowRouter.go('/home');
	}
});

//Route to notification
FlowRouter.route('/notification', {
	name: "Notifications",
	action() {
		$(window).scrollTop(0);
		log.info("Notification Loaded");
		BlazeLayout.render('commonheaderLayout', {
			main: 'notification',
			title: "Notifications"
		});
	},
	onBack: function () {
		$(window).scrollTop(0);
		FlowRouter.go('/home');
	}
});
