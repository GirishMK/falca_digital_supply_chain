// Import server startup through a single index entry point

import './fixtures.js';
import './register-api.js';

// Catching all server errors
import { log } from "../both/index.js";

const bound = Meteor.bindEnvironment((callback) => { callback(); });
process.on('uncaughtException', (err) => {
	bound(() => {
		log.error("Server Crashed!", err);	
		process.exit(7);
	});
});