// Register your apis here

// Registering all the publications and methods here of all the apis for easier access
import '../../api/tblPublish/methods.js';
import '../../api/tblPublish/server/publications.js';

import '../../api/users/methods.js';
import '../../api/users/server/publications.js';

import '../../api/tblProducts/methods.js';
import '../../api/tblProducts/server/publications.js';

import '../../api/tblNotification/methods.js';
import '../../api/tblNotification/server/publications.js';

import '../../api/extras/constants.js';
import '../../api/extras/methods.js';


import '../../api/tblStateDistricts/server/publications.js';

import '../../api/tblAndroidAppstoreAnalytics/server/publications.js';


import '../../api/tblReleaseVersion/methods.js';
import '../../api/tblReleaseVersion/server/publications.js';
